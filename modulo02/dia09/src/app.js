const urlApi = 'https://pokeapi.co/api/v2/pokemon/'
const pokeApi = new PokeApi(urlApi)

let app = new Vue({
  el: '#meuPrimeiroApp',
  data: {
    idPokemon: '',
    pokemonAtual: null,
    pokemon: { },
    pokemonValido: false
  },
  methods: {
    async buscar() {
      if (isNaN(parseInt(this.idPokemon))) {
        this.pokemonValido = false
        return
      }

      if (this.idPokemon === this.pokemonAtual) return

      this.pokemonAtual = this.idPokemon
      this.pokemon = await pokeApi.buscar(this.pokemonAtual)
      this.pokemonValido = true
    },
    async buscarRandomico() {
      if(!localStorage.numerosSorteados) localStorage.numerosSorteados = '0,'

      listaNumeros = localStorage.numerosSorteados.split(',')
      do {
        this.idPokemon = Math.floor(Math.random() * 803)
      } while (listaNumeros.indexOf(this.idPokemon.toString()) != -1)

      localStorage.numerosSorteados += `${this.idPokemon},`
      this.pokemonAtual = this.idPokemon
      this.pokemon = await pokeApi.buscar(this.pokemonAtual)
      this.pokemonValido = true
    }
  }
})
