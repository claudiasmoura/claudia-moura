/*
Exercício 01

Altere o comportamento de clique do botão “Limpar dados”
para remover a chave “nome” do localStorage, quando ele for clicado.

Exercício 02

No clique do botão “Limpar dados”, adicione uma verificação
 para desabilitar o botão (pesquise como fazer isso) após 5
 cliques, independente se a página foi recarregada ou não.
*/

const h2Titulo = document.getElementById("titulo")
const botaoApagar = document.getElementById("btn-apagar")
botaoApagar.onclick = apagaNome
verificaCliquesBtnApagar()

if (!localStorage.nome || localStorage.nome === '') {
  const nome = prompt("Digite seu nome")
  salvaNome(nome)
}

exibeNome()

function salvaNome(nome) {
  localStorage.nome = nome || 'ninguém'
}

function exibeNome() {
  h2Titulo.innerText = `Olá, ${localStorage.nome}!`
}

function apagaNome() {
  delete localStorage.nome
  h2Titulo.innerText = `Olá!`
  contabilizaCliquesBtnApagar()
}

function verificaCliquesBtnApagar() {
  botaoApagar.disabled = true
  if (!localStorage.cliquesBtnApagar || localStorage.cliquesBtnApagar < 5) {
    botaoApagar.disabled = false
  }
}

function contabilizaCliquesBtnApagar() {
  if (!localStorage.cliquesBtnApagar) localStorage.cliquesBtnApagar = 0
  localStorage.cliquesBtnApagar++
  verificaCliquesBtnApagar()
}
