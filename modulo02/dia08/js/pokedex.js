function rodarPrograma() {
  const $dadosPokemon = document.getElementById('dados-pokemon')
  const $btnSorte = document.getElementById('btn-sorte')
  const $h2 = document.querySelector('h2')
  const $h1 = $dadosPokemon.querySelector('h1')
  const $img = $dadosPokemon.querySelector('img')
  const $id = document.getElementById('id')
  const $altura = document.getElementById('altura')
  const $peso = document.getElementById('peso')
  const $tipo = document.getElementById('tipo')
  const $estatistica = document.getElementById('estatisticas')
  const idPokemon = document.getElementById('numero-pokemon')

  const url = 'https://pokeapi.co/api/v2/pokemon/'
  const pokeApi = new PokeApi(url)

  let numero = null
  if(!localStorage.numerosSorteados) localStorage.numerosSorteados = '0,'

  $dadosPokemon.hidden = true

  $btnSorte.onclick = function () {
    idPokemon.value = ''
    listaNumeros = localStorage.numerosSorteados.split(',')
    do {
      numero = Math.floor(Math.random() * 803)
      console.log(numero)
    } while (listaNumeros.indexOf(numero.toString()) != -1)

    localStorage.numerosSorteados += `${numero},`

    criarPokemon(numero)
  }

  idPokemon.onblur = function() {
    numero = idPokemon.value

    if (isNaN(parseInt(numero))) {
      $h2.innerText = 'Digite um id válido'
      $dadosPokemon.hidden = true
      return
    }

    if (idPokemon.value === numero) return

    criarPokemon(numero)
  }

  function criarPokemon(numero) {
    pokeApi.buscar(numero)
    .then(res => res.json())
    .then(dadosJson => {
      const pokemon = new Pokemon(dadosJson)
      exibirPokemon(pokemon)
    })
  }

  function exibirPokemon(pokemon) {

    $h2.innerText = ''
    $dadosPokemon.hidden = false

    $h1.innerText = pokemon.nome
    $img.src = pokemon.thumbUrl
    $img.alt = pokemon.nome
    $id.innerText = pokemon.id
    $altura.innerText = ` ${pokemon.getAltura()}cm`
    $peso.innerText = ` ${pokemon.getPeso()}kg`
    pokemon.tipos.map(function(item) {
      let li = criarElemento('li')
      li.innerText = item
      insere(li, $tipo)
    })
    pokemon.status.map(function(item) {
      let li = criarElemento('li')
      li.innerText = item[0]
      let div = criarElemento('div')
      div.className = `status-bar`
      div.style.width = `${item[1]}%`
      div.innerText = `${item[1]}`
      insere(div, li)
      insere(li, $estatistica)
    })
  }

  function criarElemento(elemento) {
      return document.createElement(elemento)
  }

  function insere(filho, elemento) {
    return elemento.appendChild(filho)
  }
}

rodarPrograma()
