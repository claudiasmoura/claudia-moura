class Jedi {
  constructor(nome) {
    this.nome = nome

    this.h1 = document.createElement('h1')
    this.h1.innerText = this.nome
    this.h1.id = `jedi_${this.nome} ` 
    const dadosJedi = document.getElementById('dados-jedi')
    dadosJedi.appendChild(this.h1)
  }

  atacar() {
    let that = this
    setTimeout(function() { 
      that.h1.innerText += atacou
    }, 1000)
  }

  atacarBind() {
    setTimeout(function() { 
      this.h1.innerText += atacou
    }.bind(this), 1000)
  }

  atacarArrowFunction() {
    setTimeout( () => { 
      this.h1.innerText += atacou
    },)
  }
}
