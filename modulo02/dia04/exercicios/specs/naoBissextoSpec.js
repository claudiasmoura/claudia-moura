describe('naoBissexto', function() {

  it('ano 2016 deve retornar false', function() {
    const resultado = naoBissexto(2016)
    chai.expect(resultado).to.be.false
  })

  it('ano 2015 deve retornar true', function() {
    const resultado = naoBissexto(2015)
    chai.expect(resultado).to.be.true
  })

  it('ano 0 deve retornar false', function() {
    const resultado = naoBissexto(4)
    chai.expect(resultado).to.be.false
  })

  it('ano 1 deve retornar true', function() {
    const resultado = naoBissexto(1)
    chai.expect(resultado).to.be.true
  })
})
