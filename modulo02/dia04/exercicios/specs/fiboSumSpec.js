describe('fiboSum', function() {

  beforeEach(function() {
    chai.should()
  })

  it('Soma os primeiros 7 elementos', function() {
    const resultado = fiboSum(7)
    resultado.should.equal(33)
  })

  it('Soma os primeiros 20 elementos', function() {
    const resultado = fiboSum(20)
    resultado.should.equal(17710)
  })
})
