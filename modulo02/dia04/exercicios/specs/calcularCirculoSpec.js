describe('calcularCirculo', function() {

  beforeEach(function() {
    chai.should()
  })

  it('deve calcular área de raio 3', function() {
    const raio = 3, tipoCalculo = 'A'
    const resultado = calcularCirculo({ raio, tipoCalculo })
    resultado.should.equal(28.274333882308138)
  })

  it('deve calcular perímetro de raio 3', function() {
    const raio = 3, tipoCalculo = 'C'
    const resultado = calcularCirculo({ raio, tipoCalculo })
    resultado.should.be.closeTo(18.849, 0.001)
  })
})
