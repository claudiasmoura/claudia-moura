describe('concatenarSemUndefined', function() {

  beforeEach(function() {
    chai.should()
  })

  it('Primeiro parâmetro undefined', function() {
    const resultado = concatenarSemUndefined(undefined, 'Soja')
    resultado.should.equal('Soja')
  })

  it('Segundo parâmetro undefined', function() {
    const resultado = concatenarSemUndefined('Soja é')
    resultado.should.equal('Soja é')
  })

  it('Nenhum parâmetro undefined', function() {
    const resultado = concatenarSemUndefined('Soja', ' é bom')
    resultado.should.equal('Soja é bom')
  })

  it('Segundo parâmetro null', function() {
    const resultado = concatenarSemUndefined('Soja', null)
    resultado.should.equal('Sojanull')
  })
})

describe('concatenarSemNull', function() {

  beforeEach(function() {
    chai.should()
  })

  it('Primeiro parâmetro null', function() {
    const resultado = concatenarSemNull(null,'Soja')
    resultado.should.equal('Soja')
  })

  it('Nenhum parâmetro null', function() {
    const resultado = concatenarSemNull('Soja', ' é bom')
    resultado.should.equal('Soja é bom')
  })

  it('Segundo parâmetro undefined', function() {
    const resultado = concatenarSemNull('Soja é')
    resultado.should.equal('Soja éundefined')
  })
})

describe('concatenarEmPaz', function() {

  beforeEach(function() {
    chai.should()
  })

  it('Primeiro parâmetro undefined', function() {
    const resultado = concatenarEmPaz(undefined,'Soja')
    resultado.should.equal('Soja')
  })

  it('Segundo parâmetro null', function() {
    const resultado = concatenarEmPaz('Soja é bom, mas...')
    resultado.should.equal('Soja é bom, mas...')
  })

  it('Segundo parâmetro undefined', function() {
    const resultado = concatenarEmPaz('Soja é')
    resultado.should.equal('Soja é')
  })
})
