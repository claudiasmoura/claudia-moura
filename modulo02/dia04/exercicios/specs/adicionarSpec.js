describe('adicionar', function() {

  beforeEach(function() {
    chai.should()
  })

  it('Soma 3 e 4', function() {
    const resultado = adicionar(3)(4)
    resultado.should.equal(7)
  })

  it('Soma 5642 e 8749', function() {
    const resultado = adicionar(5642)(8749)
    resultado.should.equal(14391)
  })
})
