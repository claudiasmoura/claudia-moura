/*
Exercício 01
  Crie uma função "calcularCirculo" que receba um objeto com os seguintes parâmetros:
  {
    raio, // raio da circunferência
    tipoCalculo // se for "A" cálcula a área, se for "C" calcula a circunferência
  }
*/

const calcularCirculo = objeto => {
  if (objeto.tipoCalculo === 'A') return Math.PI * objeto.raio ** 2
  if (objeto.tipoCalculo === 'C') return Math.PI * 2 * objeto.raio
}

/*
  Exercício 02
  Crie uma função naoBissexto que recebe um ano (número) e verifica se ele não é bissexto.
*/

const naoBissexto = ano => !(ano % 4 === 0 && ano % 100 !== 0 || ano % 400 === 0 )

/*
  Exercício 03
  Crie uma função concatenarSemUndefined que recebe duas variáveis. Caso qualquer uma delas seja undefined (cuide a melhor forma de verificar isso, foi visto em aula), considere “” no lugar. Exemplo:
*/

const textoUndefined = texto => texto === undefined

const concatenarSemUndefined = (texto1, texto2) => {
  if (textoUndefined(texto1)) texto1 = ''
  if (textoUndefined(texto2)) texto2 = ''
  return texto1 + texto2
}
/*
  Exercício 04
  Crie uma função concatenarSemNull que recebe duas variáveis. Caso qualquer uma delas seja null (cuide a melhor forma de verificar isso, foi visto em aula), considere “” no lugar. Exemplo:
*/

const textoNull = texto => texto === null

const concatenarSemNull = (texto1, texto2) => {
  if (textoNull(texto1)) texto1 = ''
  if (textoNull(texto2)) texto2 = ''
  return texto1 + texto2
}

/*
  Exercício 05
  Crie uma função concatenarEmPaz que recebe duas variáveis. Caso qualquer uma delas seja null ou undefined (cuide a melhor forma de verificar isso, foi visto em aula), considere “” no lugar. Exemplo
*/

const concatenarEmPaz = (texto1, texto2) => {
  texto1 = texto1 || ''
  texto2 = texto2 || ''
  return texto1 + texto2
}

/*
  Exercício 06 - Soma diferentona
  Escreva uma função adicionar que permite somar dois números através de duas chamadas diferentes (não necessariamente pra mesma função). Pirou? Ex:
*/

const adicionar = numero1 => numero2 => numero1 + numero2

/*
  Exercício 07 - Fibonacci
  Faça uma função fiboSum que calcule a soma da sequência de Fibonacci para n números informados. Exemplo de chamada:
  fiboSum(7)
  // 33 (soma dos 7 primeiros números da sequência: 1+1+2+3+5+8+13)
  Dica: aproveite toda "beleza" dos algoritmos recursivos (com cuidado).
*/

const fibonacci = numero => {
  if (numero === 1 || numero === 2) return 1
  return fibonacci(numero - 2) + fibonacci(numero - 1)
}

const fiboSum = posicao => {
  if (posicao === 1) return 1

  const soma = fibonacci(posicao)
  return soma + fiboSum(posicao - 1)
}
