import Pokemon from '../models/pokemon.js'

export default class PokeApi {
  constructor( url ) {
    this.url = url
  }

  async listarPorTipo( idTipo, qtdePokemon = 10, pagina = 1 ) {
    return new Promise(resolve => {
      const urlTipo = `https://pokeapi.co/api/v2/type/${ idTipo }/`
      fetch( urlTipo )
      .then( j => j.json())
      .then( t => {
        const pokemons = this.paginarPokemon(t.pokemon, qtdePokemon, pagina)
        const promisesPokemon = pokemons.map( p => this.buscarPorUrl( p.pokemon.url ))
        Promise.all( promisesPokemon ).then( resultadoFinal => {
          resolve (resultadoFinal)
        })
      })
    })
  }
  
  paginarPokemon(lista, qtdePokemon, pagina) {
    let inicio, fim
    pagina >= 2 ? inicio = qtdePokemon * (pagina - 1) : inicio = 0
    fim = inicio + qtdePokemon
    return lista.slice( inicio, fim )
  }

  async buscarPorUrl( urlPokemon ) {
    return new Promise( resolve => {
      fetch( urlPokemon )
        .then( j => j.json() )
        .then( p => {
          const pokemon = new Pokemon( p )
          resolve( pokemon )
        })
    })
  }

  async buscar( idPokemon ) {
    let urlPokemon = `${ this.url }/${ idPokemon }/`
    return this.buscarPorUrl( urlPokemon )
  }
}
