import Pokemon from '../models/pokemon.js'

export default class PokeApi {
  constructor(url) {
    this.url = url
  }

  // async listarPorTipo(idTipo) {
  // }

  async buscar(idPokemon) {
    let urlPokemon = `${this.url}/${idPokemon}/`
    return new Promise(resolve => {
      fetch(urlPokemon)
      .then(j => j.json())
      .then(p => {
        const pokemon = new Pokemon(p)
        resolve(pokemon)
      })
    })
  }
}
