export default class Pokemon {
  constructor(jsonVindoDaApi) {
    this.nome = jsonVindoDaApi.name
    this.id = jsonVindoDaApi.id
    this.thumbUrl = jsonVindoDaApi.sprites.front_default
    this.altura = jsonVindoDaApi.height
    this.peso = jsonVindoDaApi.weight
    this.tipos = jsonVindoDaApi.types.map(t => t.type.name)
    this.status = jsonVindoDaApi.stats
  }

  getAltura() {
    return this.altura * 10
  }
  getPeso() {
    return this.peso / 100
  }
}
