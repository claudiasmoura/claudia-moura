//Crie uma função somarPosiçãoPares que recebe um array de números (não precisa fazer validação)
//e soma todos números nas posições pares do array, exemplo:

const somarPosicoesPares = array => array.reduce((soma, item, index) => index % 2 === 0 ? soma + item : soma)


/*
Exercício 02
Crie uma função formatarElfos que recebe um array de objetos e retorna o mesmo array com as
seguintes transformações de campos:

const elfo1 = { nome: "Legolas", experiencia: 0, qtdFlechas: 6 }
const elfo2 = { nome: "Galadriel", experiencia: 1, qtdFlechas: 1 }
const arrayElfos = [ elfo1, elfo2 ]
formatarElfos( [ elfo1, elfo2 ] ) // retorna o seguinte array:
	[{nome: "LEGOLAS", temExperiencia: false,	qtdFlechas: 6,	descricao: "LEGOLAS sem experiência e com 6 flechas" },
  {nome: "GALADRIEL",temExperiencia: true,qtdFlechas: 1,descricao: "GALADRIEL com experiência e 1 flecha" }]
*/

const formatarElfos = elfos => {
  elfos.forEach(elfo => {
    let xp, flechas
    elfo.nome = elfo.nome.toUpperCase()
    elfo['temExperiencia'] = elfo['experiencia'];
    delete elfo['experiencia'];
    elfo.temExperiencia > 0 ? elfo.temExperiencia = true : elfo.temExperiencia = false
    elfo.temExperiencia ? xp = 'com experiência e' : xp = 'sem experiência e com'
    elfo.qtdFlechas === 1 ? flechas = 'flecha' : flechas = 'flechas'
    elfo.descricao = `${elfo.nome} ${xp} ${elfo.qtdFlechas} ${flechas}`
    })
  return elfos
}

