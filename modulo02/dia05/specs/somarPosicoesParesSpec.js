describe('somarPosicoesPares', function() {
   beforeEach( function() {
       chai.should()
     })

     it('deve somar apenas numeros em posições pares', function() {
         const resultado = somarPosicoesPares([1, 56, 4.34, 6, -2])
         resultado.should.equal(3.34)
     })

     it('somar numeros em posicões pares com todos negativos', function() {
       const resultado = somarPosicoesPares([-1.1, -2, -3.3, -4, -5.4, -6, -7.22])
       resultado.should.equal(-17.02)
     })

     it('somar numeros em posições pares com todos positivos', function() {
       const resultado = somarPosicoesPares([0, 1.54, 2, 3.768, 4, 5.333, 6, 7.1])
       resultado.should.equal(12)
     })
})
