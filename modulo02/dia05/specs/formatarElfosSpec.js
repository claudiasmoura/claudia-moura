describe('Formatar Elfos', function() {
  beforeEach( function() {
    chai.should()
  })

  it('todos os elfos recebem descricao', function() {
    const elfo1 = { nome: "Legolas", experiencia: 0, qtdFlechas: 6 }
    const elfo2 = { nome: "Galadriel", experiencia: 1, qtdFlechas: 1 }
    const arrayElfos = [ elfo1, elfo2 ]
    const resultado = formatarElfos(arrayElfos)
    resultado.should.deep.equal([{
  		nome: "LEGOLAS",
  		temExperiencia: false,
  		qtdFlechas: 6,
  		descricao: "LEGOLAS sem experiência e com 6 flechas"
  	  }, {
  		nome: "GALADRIEL",
  		temExperiencia: true,
  		qtdFlechas: 1,
  		descricao: "GALADRIEL com experiência e 1 flecha"
  	}])
  })

  it('elfos com experiência false recebem descricao sem experiência', function() {
    const elfo = { nome: "Legolas", experiencia: 0, qtdFlechas: 6 }
    const arrayElfos = [ elfo ]
    const descricao = formatarElfos(arrayElfos)[0].descricao
    descricao.should.equal("LEGOLAS sem experiência e com 6 flechas")
  })

  it('elfos com experiência true recebem descricao com experiência', function() {
    const elfo = { nome: "Galadriel", experiencia: 1, qtdFlechas: 1 }
    const arrayElfos = [ elfo ]
    const descricao = formatarElfos(arrayElfos)[0].descricao
    descricao.should.equal("GALADRIEL com experiência e 1 flecha")
  })

  it('elfos recebem descricao com quantidade de flechas', function() {
    const elfo1 = { nome: "Legolas", experiencia: 0, qtdFlechas: 15 }
    const elfo2 = { nome: "Galadriel", experiencia: 1, qtdFlechas: 22 }
    const arrayElfos = [ elfo1, elfo2 ]
    const resultado = formatarElfos(arrayElfos)
    resultado.should.deep.equal([{
  		nome: "LEGOLAS",
  		temExperiencia: false,
  		qtdFlechas: 15,
  		descricao: "LEGOLAS sem experiência e com 15 flechas"
  	  }, {
  		nome: "GALADRIEL",
  		temExperiencia: true,
  		qtdFlechas: 22,
  		descricao: "GALADRIEL com experiência e 22 flechas"
  	}])
  })

  it('elfos com 1 flecha recebem descricao com quantidade de flechas no singular', function() {
    const elfo = { nome: "Galadriel", experiencia: 1, qtdFlechas: 1 }
    const arrayElfos = [ elfo ]
    const descricao = formatarElfos(arrayElfos)[0].descricao
    descricao.should.equal("GALADRIEL com experiência e 1 flecha")
  })

  it('formatarElfos retorna mesmo array', function() {
    const elfo1 = { nome: "Legolas", experiencia: 0, qtdFlechas: 6 }
    const elfo2 = { nome: "Galadriel", experiencia: 1, qtdFlechas: 1 }
    const arrayElfos = [ elfo1, elfo2 ]
    formatarElfos(arrayElfos).should.equal(arrayElfos)
  })
})
