
class Relogio {

  getData() {
    return new Date().toLocaleTimeString()
  }

  iniciar(func) {
    setInterval(func, 1000)
  }

  parar() {
    clearInterval(this.iniciarRelogio, 1000)
  }
}


const relogio = new Relogio()
