package br.com.dbc.petshopjpa.dao;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author claudia.moura
 */
public class PersistenceUtils {
    private static EntityManager em;
    
    static {
        em = Persistence
                .createEntityManagerFactory("petshop")
                .createEntityManager();
    }
    
    public static EntityManager getEntityManager() {
        return em;
    }
}
