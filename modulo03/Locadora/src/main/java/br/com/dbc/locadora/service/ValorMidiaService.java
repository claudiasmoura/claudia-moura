package br.com.dbc.locadora.service;

import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.enums.MidiaType;
import br.com.dbc.locadora.repository.MidiaRepository;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author claudia.moura
 */
@Service
public class ValorMidiaService extends AbstractCrudService<ValorMidia> {

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;
    
    @Autowired
    private MidiaRepository midiaService;

    @Override
    protected JpaRepository<ValorMidia, Long> getRepository() {
        return valorMidiaRepository;
    }

    public ValorMidia findByIdMidiaAndRetirada(Long idMidia, LocalDateTime retirada) {
        return valorMidiaRepository.findByIdMidiaAndRetirada(idMidia, retirada);
    }
    
    public List<ValorMidia> findByMidia(Midia midia) {
        return valorMidiaRepository.findByMidia(midia);
    }
    
    public List<ValorMidia> findByMidiaAndMidiaTipo(Midia midia, MidiaType tipo) {
        return valorMidiaRepository.findByMidiaAndMidiaTipo(midia, tipo);
    }    
}
