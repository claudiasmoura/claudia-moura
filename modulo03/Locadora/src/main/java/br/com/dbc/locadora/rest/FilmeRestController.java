package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.dto.CatalogoDTO;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.enums.Categoria;
import br.com.dbc.locadora.service.FilmeService;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author claudia.moura
 */
@PreAuthorize("hasAuthority('ADMIN_USER')")
@RestController
@RequestMapping("/api/filme")
public class FilmeRestController extends AbstractRestController<Filme, FilmeService> {
    
    @Autowired
    private FilmeService filmeService;
    
    @Override
    protected FilmeService getService() {
        return filmeService;
    }
    
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @PostMapping("/midia")
    public ResponseEntity<?> salvarComMidia(@RequestBody FilmeDTO dto) {
        return ResponseEntity.ok(filmeService.salvarComMidia(dto));
    }
    
    @RequestMapping(value="/search", method = RequestMethod.GET)
    public ResponseEntity<Page<Filme>> findByTituloOrCategoriaOrLancamento(
        Pageable pageable,
        @RequestParam(value = "titulo", required = false) String titulo,
        @RequestParam(value = "categoria" , required = false) Categoria categoria,
        @RequestParam(value = "lancamento", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate lancamento
    ) {
       return ResponseEntity.ok(filmeService.findByTituloOrCategoriaOrLancamento(pageable, titulo, categoria, lancamento)); 
    }
    
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @PutMapping("/{id}/midia")
    public ResponseEntity<?> atualizarComMidia(@RequestBody FilmeDTO dto) {
        return ResponseEntity.ok(filmeService.atualizarComMidia(dto));
    }
    
    @PostMapping("/search/catalogo")
    public ResponseEntity<Page<CatalogoDTO>> buscarCatalogo(Pageable pageable, @RequestBody FilmeDTO filmeDTO){
        return ResponseEntity.ok(getService().buscarCatalogo(pageable, filmeDTO));
    }  
}
