package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.enums.MidiaType;
import br.com.dbc.locadora.repository.MidiaRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author claudia.moura
 */

@Service
public class MidiaService extends AbstractCrudService<Midia>{
    
    @Autowired
    private MidiaRepository midiaRepository;  
    
    @Autowired
    private ValorMidiaService valorMidiaService;
    
    @Override
    protected JpaRepository<Midia, Long> getRepository() {
        return  midiaRepository;
    }
    
    @Transactional(readOnly = false)
    public void saveMidiaDTO(MidiaDTO dto, Filme filme){
        for (int i = 0; i < dto.getQuantidade(); i++) {              
            Midia midia = dto.transformaParaMidia(filme);                    
            midiaRepository.save(midia);
            valorMidiaService.save(new ValorMidia(null, dto.getValor(), LocalDateTime.now(), null, midia));
        }        
    }
    
    @Transactional(readOnly = false)
    public void updateComFilme(Long id, MidiaDTO input, Filme filme) {
        
        HashMap<MidiaType, ArrayList<Midia>> porTipo = new HashMap<>(); 
        List<Midia> midiasAtuais = midiaRepository.findByFilme(filme);
        
        midiasAtuais.forEach((midia) -> {
            if (porTipo.get(midia.getTipo()) == null) {
                ArrayList<Midia> midiaPorTipo = new ArrayList<>();
                porTipo.put(midia.getTipo(), midiaPorTipo);
            }        
            porTipo.get(midia.getTipo()).add(midia);         
        });
        
        porTipo.get(input.getTipo()).forEach((midia) -> {     
            input.setId(midia.getId());
            this.save(input.transformaParaMidia(filme));            
        });        
  
        int tamanhoListaAtual = porTipo.get(input.getTipo()).size();            
        int diferenca = tamanhoListaAtual - input.getQuantidade();
        
        if (diferenca > 0) {
            for (int i = 0; i < diferenca; i++ ) {
                valorMidiaService.findByMidia(porTipo.get(input.getTipo()).get(i)).forEach((valorMidia) -> {
                    porTipo.get(input.getTipo()).remove(valorMidia);
                    valorMidiaService.delete(valorMidia.getId());
                    input.setQuantidade(input.getQuantidade() + 1);
                });
                midiaRepository.deleteById(porTipo.get(input.getTipo()).get(i).getId());                
            }
        }
        
        if (diferenca < 0) {
            for (int i = diferenca; i < 0; i++ ) {
                input.setId(null);
                midiaRepository.save(input.transformaParaMidia(filme));
                input.setQuantidade(input.getQuantidade() - 1);
            }
        }        
    }    
    
    public List<Midia> findByFilmeAndTipo(Filme filme, MidiaType tipo) {
        return midiaRepository.findByFilmeAndTipo(filme, tipo);
    }
    
    public Long countByTipo(MidiaType tipo){
        return new Long(midiaRepository.countByTipo(tipo));
    }    

    public List<Midia> findByAluguel(Long id) {
        return midiaRepository.findByAluguel(id);
    }
    
    public List<Midia> findByFilme(Filme filme) {
        return midiaRepository.findByFilme(filme);
    }
}
