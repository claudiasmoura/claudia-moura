package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.service.AluguelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author claudia.moura
 */
@PreAuthorize("hasAuthority('ADMIN_USER')")
@RestController
@RequestMapping("/api/aluguel")
public class AluguelRestController extends AbstractRestController<Aluguel, AluguelService> {
    
    @Autowired
    private AluguelService aluguelService;

    @Override
    protected AluguelService getService() {
        return aluguelService;
    }    
    
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @PostMapping("/retirada")
    public ResponseEntity<?> salvarAluguel(@RequestBody AluguelDTO input) {        
        return ResponseEntity.ok(getService().salvarAluguel(input));
    }
    
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @PostMapping("/devolucao")
    public ResponseEntity<?> devolverMidia(@RequestBody AluguelDTO input) {        
        return ResponseEntity.ok(getService().devolver(input));
    }
    
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @GetMapping("/devolucao")
    public ResponseEntity<Page<Filme>> devolucaoAluguelHoje (Pageable pageable){
        return ResponseEntity.ok(aluguelService.devolucaoHoje(pageable));        
    }
}
