package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.enums.MidiaType;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author claudia.moura
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MidiaDTO {
    
    private Long id;
    private  MidiaType tipo;
    private int quantidade;
    private double valor;
    
    public Midia transformaParaMidia(Filme filme){
        return new Midia(id, tipo, filme, null);
    }        
}
