package br.com.dbc.locadora.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author claudia.moura
 */
@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValorMidia implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "ID", nullable = false)
    private Long id;
    
    @Column(name = "VALOR", nullable = false, precision = 10, scale = 2)
    private Double valor;
    
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "INICIO_VIGENCIA", nullable = false)
    private LocalDateTime inicioVigencia;
    
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
    private LocalDateTime fimVigencia;
    
    @JoinColumn(name = "ID_MIDIA", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Midia midia;    

}