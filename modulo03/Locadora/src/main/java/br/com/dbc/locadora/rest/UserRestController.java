package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.User;
import br.com.dbc.locadora.service.AppUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author claudia.moura
 */
@PreAuthorize("hasAuthority('ADMIN_USER')")
@RestController
@RequestMapping("/api/user")
public class UserRestController {
    
    @Autowired
    private AppUserDetailsService userService;

   
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @PostMapping
    public ResponseEntity<?> criarUsuario(@RequestBody User user) {        
        return ResponseEntity.ok(userService.salvarUsuario(user));
    }
    
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @PostMapping("/password")
    public ResponseEntity<?> mudarSenha(@RequestBody User user) {        
        return ResponseEntity.ok(userService.alterarSenha(user));
    }
}
