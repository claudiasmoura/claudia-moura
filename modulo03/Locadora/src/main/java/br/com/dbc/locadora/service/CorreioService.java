package br.com.dbc.locadora.service;

import br.com.dbc.locadora.config.SoapConnector;
import br.com.dbc.locadora.dto.EnderecoDTO;
import br.com.dbc.locadora.ws.ConsultaCEP;
import br.com.dbc.locadora.ws.ConsultaCEPResponse;
import br.com.dbc.locadora.ws.ObjectFactory;
import javax.xml.bind.JAXBElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author claudia.moura
 */

@Service
public class CorreioService {
    
    @Autowired
    private SoapConnector soapConnector;
    
    @Autowired 
    private ObjectFactory objectFactory;
    
    public EnderecoDTO buscar(String cep){
        
        ConsultaCEP consultaCep = objectFactory.createConsultaCEP();
        
        consultaCep.setCep(cep);
        
        ConsultaCEPResponse resposta = ((JAXBElement<ConsultaCEPResponse>) 
                soapConnector.callWebService(objectFactory.createConsultaCEP(consultaCep))).getValue();
        
        return EnderecoDTO.builder()
                .rua(resposta.getReturn().getEnd())
                .bairro(resposta.getReturn().getBairro())
                .cidade(resposta.getReturn().getCidade())
                .UF(resposta.getReturn().getUf())
                .build();
    }
}