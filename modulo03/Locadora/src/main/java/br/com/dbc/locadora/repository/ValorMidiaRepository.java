package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.enums.MidiaType;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
/**
 *
 * @author claudia.moura
 */
public interface ValorMidiaRepository extends JpaRepository<ValorMidia, Long> {
    
    public Page<ValorMidia> findByMidiaId(Pageable pageable, Long id);
    
    @Query("select vm from ValorMidia vm where vm.midia.id = :idmidia and vm.inicioVigencia >= :retirada and (vm.fimVigencia <= :retirada or vm.fimVigencia is null)")
    public ValorMidia findByIdMidiaAndRetirada(@Param("idmidia")Long idMidia, @Param("retirada")LocalDateTime retirada);
    
    public List<ValorMidia> findByMidia(Midia midia);

    public List<ValorMidia> findByMidiaAndMidiaTipo(Midia midia, MidiaType tipo);
    
}
