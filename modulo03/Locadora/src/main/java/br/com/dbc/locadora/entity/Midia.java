package br.com.dbc.locadora.entity;

import br.com.dbc.locadora.enums.MidiaType;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author claudia.moura
 */
@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Midia implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "ID", nullable = false)
    private Long id;
    
    @Column(name = "MIDIA_TYPE", nullable = false)
    private MidiaType tipo;
    
    @JoinColumn(name = "ID_FILME", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Filme filme;    
    
    @JoinColumn(name = "ID_ALGUEL", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    private Aluguel aluguel;
}
