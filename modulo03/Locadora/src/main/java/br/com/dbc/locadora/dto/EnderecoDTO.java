package br.com.dbc.locadora.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author claudia.moura
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@Data
public class EnderecoDTO {
    private String rua;
    private String bairro;
    private String cidade;
    private String UF;    
}
