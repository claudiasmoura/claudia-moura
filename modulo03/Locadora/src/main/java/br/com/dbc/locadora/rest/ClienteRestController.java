package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.config.SoapConnector;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.service.ClienteService;
import br.com.dbc.locadora.service.CorreioService;
import br.com.dbc.locadora.ws.ObjectFactory;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author claudia.moura
 */
@PreAuthorize("hasAuthority('ADMIN_USER')")
@RestController
@RequestMapping("/api/cliente")
public class ClienteRestController extends AbstractRestController<Cliente, ClienteService> {
    
    @Autowired
    private ClienteService clienteService;
    
    @Autowired
    private CorreioService correioService;
    
    @Override
    protected ClienteService getService() {
        return clienteService;
    }    
    
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @PutMapping("/{id}")
    @Override
    public ResponseEntity<?> put(@PathVariable Long id, @Valid @RequestBody Cliente input) {
        return ResponseEntity.ok(clienteService.save(input));
    }
    
    @Autowired SoapConnector soapConnector;
    @Autowired ObjectFactory objectFactory;    
    @GetMapping("/cep/{cep}")
    public ResponseEntity<?> get(@PathVariable String cep) {
        return ResponseEntity.ok(correioService.buscar(cep));
    }
}
