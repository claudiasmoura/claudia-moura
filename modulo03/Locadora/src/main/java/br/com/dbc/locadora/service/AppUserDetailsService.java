package br.com.dbc.locadora.service;

import br.com.dbc.locadora.entity.User;
import br.com.dbc.locadora.repository.RoleRepository;
import br.com.dbc.locadora.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by nydiarra on 06/05/17.
 */
@Component
public class AppUserDetailsService extends AbstractCrudService<User> implements UserDetailsService  {
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private RoleRepository roleRepository;
    
    @Autowired
    private BCryptPasswordEncoder bPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(s);

        if(user == null) {
            throw new UsernameNotFoundException(String.format("The username %s doesn't exist", s));
        }

        List<GrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
        });

        UserDetails userDetails = new org.springframework.security.core.userdetails.
                User(user.getUsername(), user.getPassword(), authorities);

        return userDetails;
    }
    
    @Transactional(readOnly = false)
    public User salvarUsuario(User user) {
        user.setRoles(Arrays.asList(roleRepository.findByRoleName("STANDARD_USER")));
        return userRepository.save(user);
    }
    
    @Transactional(readOnly = false)
    public User alterarSenha(User input) {
        User user = userRepository.findByUsername(input.getUsername());
        user.setPassword(input.getPassword());
        user.setPassword(bPasswordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    protected JpaRepository<User, Long> getRepository() {
        return userRepository;
    }
}
