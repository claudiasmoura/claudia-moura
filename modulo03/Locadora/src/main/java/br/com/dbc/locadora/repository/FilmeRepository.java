package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.enums.Categoria;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author claudia.moura
 */
public interface FilmeRepository extends JpaRepository<Filme, Long> {
    
    
    public Page<Filme> findByTituloOrCategoriaOrLancamentoIgnoreCaseContaining(
            Pageable pageable,
            String titulo, 
            Categoria categoria,
            LocalDate lancamento
    );
    
    public List<Filme> findByTituloIgnoreCaseContaining(String titulo);
    
}
