package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.AluguelRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author claudia.moura
 */
@Service
public class AluguelService extends AbstractCrudService<Aluguel> {

    @Autowired
    private AluguelRepository aluguelRepository;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private MidiaService midiaService;

    @Autowired
    private ValorMidiaService valorMidiaService;
    
    @Autowired
    private FilmeService filmeService;

    @Override
    protected JpaRepository<Aluguel, Long> getRepository() {
        return aluguelRepository;
    }

    @Transactional(readOnly = false)
    public Aluguel salvarAluguel(@RequestBody AluguelDTO dto) {
        Aluguel aluguel = save(dto.transformaParaAluguel(clienteService.findById(dto.getIdCliente()).get()));
        dto.getMidias().forEach((midiaDTO) -> {
            midiaService.findById(midiaDTO).get().setAluguel(aluguel);
        });
        
        return aluguel;
    }

    @Transactional(readOnly = false)
    public AluguelDTO devolver(@RequestBody AluguelDTO dto) {
        Aluguel aluguel = midiaService.findById(dto.getMidias().get(0)).get().getAluguel();
        aluguel.setDevolucao(LocalDateTime.now());
        List<ValorMidia> valoresMidia = new ArrayList<>();
        dto.getMidias().forEach((Long idMidia) -> {
            Midia midia = midiaService.findById(idMidia).get();
            midia.setAluguel(null);
            ValorMidia valorMidia = valorMidiaService.findByIdMidiaAndRetirada(idMidia, aluguel.getRetirada());
            valoresMidia.add(valorMidia);
            midiaService.save(midia);
        });        
        Double valor = valoresMidia.stream().mapToDouble(ValorMidia::getValor).sum();
        aluguel.setMulta(0.);
        if (aluguel.getDevolucao().isAfter(aluguel.getPrevisao())) {
            aluguel.setMulta(valor);
        }
        this.save(aluguel);
        dto.setValor(valor);
        dto.setMulta(aluguel.getMulta());
        dto.setId(aluguel.getId());
        dto.setDevolucao(aluguel.getDevolucao());
        dto.setIdCliente(aluguel.getCliente().getId());
        dto.setRetirada(aluguel.getRetirada());
        dto.setPrevisao(aluguel.getPrevisao());
        return dto;
    }
    
    public Page<Filme> devolucaoHoje(Pageable pageable) {
        LocalDateTime hoje = LocalDate.now().atStartOfDay();
        LocalDateTime previsao = LocalDate.now().atTime(16, 0);
        
        List<Aluguel> listaHoje = aluguelRepository.findByPrevisaoBetween(pageable, hoje, previsao).getContent();
        
        List<Midia> midias = new ArrayList<>();
        listaHoje.forEach((aluguel) -> {
            midias.addAll(midiaService.findByAluguel(aluguel.getId()));
        });
        
        List<Filme> devolucao = new ArrayList<>();
        
        midias.forEach((midia) ->{
            devolucao.add(filmeService.findById(midia.getFilme().getId()).get());
        });
        
        return new PageImpl<>(devolucao, pageable, devolucao.size());
    }
}
