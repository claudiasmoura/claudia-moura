package br.com.dbc.locadora.entity;

import br.com.dbc.locadora.enums.Categoria;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author claudia.moura
 */
@Entity
@Builder
@Data
@NoArgsConstructor 
@AllArgsConstructor
public class Filme implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "ID", nullable = false)
    private Long id;
    
    @Length(max = 200, min = 1)
    @Column(name = "TITULO", nullable = false)
    private String titulo;

    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Column(name = "LANCAMENTO", nullable = false)
    private LocalDate lancamento;
    
    @Column(name = "CATEGORIA", nullable = false)
    private Categoria categoria;
    
}
