package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.service.ValorMidiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author claudia.moura
 */
@PreAuthorize("hasAuthority('ADMIN_USER')")
@RestController
@RequestMapping("/api/valormidia")
public class ValorMidiaRestController extends AbstractRestController<ValorMidia, ValorMidiaService> {
    
    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    protected ValorMidiaService getService() {
        return valorMidiaService;
    }
}
