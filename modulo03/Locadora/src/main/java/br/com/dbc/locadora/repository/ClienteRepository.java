package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author claudia.moura
 */
public interface ClienteRepository extends JpaRepository<Cliente, Long>{
    
}

