package br.com.dbc.locadora.service;

import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author claudia.moura
 */
@Service
public class ClienteService extends AbstractCrudService<Cliente>{
    
    @Autowired
    private ClienteRepository clienteRepository;    
    
    @Override
    protected JpaRepository<Cliente, Long> getRepository() {
        return clienteRepository;
    }    
}
