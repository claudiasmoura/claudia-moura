package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author claudia.moura
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
    
    public Role findByRoleName(String name);
}
