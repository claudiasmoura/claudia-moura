package br.com.dbc.locadora.enums;

public enum MidiaType {
    VHS, DVD, BLUE_RAY
}
