package br.com.dbc.locadora.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

/**
 *
 * @author claudia.moura
 */

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cliente implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "ID", nullable = false)
    private Long id;
    
    @Length(max = 200, min = 5)
    @Column(name = "NOME", nullable = false)
    private String nome;
    
    @Length(max = 20, min = 7)
    @Column(name = "TELEFONE", nullable = false)
    private String telefone;
    
    @Length(max = 200, min = 1)
    @Column(name = "ENDERECO", nullable = false)
    private String endereco;    
}
