package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.enums.MidiaType;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author claudia.moura
 */
public interface MidiaRepository extends JpaRepository<Midia, Long> {

    public int countByTipo(MidiaType tipo);

    public List<Midia> findByAluguel(Long id);

    public List<Midia> findByFilme(Filme filme);
    
    public List<Midia> findByFilmeAndTipo(Filme filme, MidiaType tipo);

}
