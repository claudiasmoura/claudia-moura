package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.enums.MidiaType;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author claudia.moura
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MidiaCatalogoDTO {
    
    private MidiaType tipo;
    private int quantidade;
    private boolean disponivel;
    private double valor;
    private LocalDateTime previsaoDisponibilidade;    
}
