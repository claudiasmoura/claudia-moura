package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.CatalogoDTO;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.MidiaCatalogoDTO;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.enums.Categoria;
import br.com.dbc.locadora.enums.MidiaType;
import br.com.dbc.locadora.repository.FilmeRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author claudia.moura
 */
@Service
public class FilmeService extends AbstractCrudService<Filme>{
    
    @Autowired
    private FilmeRepository filmeRepository;    
    
    @Autowired
    private MidiaService midiaService; 
    
    @Autowired
    private ValorMidiaService valorMidiaService; 
        
    @Override
    protected JpaRepository<Filme, Long> getRepository() {
        return  filmeRepository;        
    }

    @Transactional(readOnly = false)
    public Filme salvarComMidia(@RequestBody FilmeDTO dto){        
        Filme filme = this.save(dto.transformaParaFilme());
        dto.getMidia().forEach((midiaDTO) -> {            
            midiaService.saveMidiaDTO(midiaDTO, filme);            
        });
        return filme;
    }
    
    @Transactional(readOnly = false)
    public FilmeDTO atualizarComMidia(@RequestBody FilmeDTO dto) {
        Filme filme = this.findById(dto.getId()).get();
        Filme filmeDto = dto.transformaParaFilme();
        this.save(filmeDto);
        
        dto.getMidia().forEach((midiaDTO) -> {            
            midiaService.updateComFilme(midiaDTO.getId(), midiaDTO, filme);            
        });
        return dto;
    }
        
        public Page<CatalogoDTO> buscarCatalogo(Pageable pageable, FilmeDTO filmeDTO) {
        List<Filme> filmes = filmeRepository.findByTituloIgnoreCaseContaining(filmeDTO.getTitulo());        
        HashMap<MidiaType, List<Midia>> listasPorTipo = new HashMap<>(); 
        List<CatalogoDTO> catalogo = new ArrayList<>();
        
        for (Filme filme: filmes) {
            for (MidiaType tipo: MidiaType.values()) {
                listasPorTipo.put(tipo, midiaService.findByFilmeAndTipo(filme, tipo));            
            }
        
            CatalogoDTO catalogoDTO = CatalogoDTO.builder()
                    .titulo(filme.getTitulo())
                    .categoria(filme.getCategoria())
                    .lancamento(filme.getLancamento())
                    .midias(new ArrayList<>())
                    .build();

            listasPorTipo.entrySet().stream().map((lista) -> {
                MidiaCatalogoDTO m = MidiaCatalogoDTO.builder()
                        .tipo(lista.getKey())
                        .quantidade(lista.getValue().size())
                        .disponivel(true)
                        .valor(valorMidiaService.findByMidia(lista.getValue().get(0)).get(0).getValor())
                        .build();
                int disponiveis = 0;
                LocalDateTime dataPrev = LocalDateTime.now().plusMonths(5l);
                for(Midia midia: lista.getValue()) {
                    if (midia.getAluguel() == null) {
                        disponiveis++;
                    } else if (midia.getAluguel().getPrevisao().isBefore(dataPrev)){
                        dataPrev = midia.getAluguel().getPrevisao();
                    }
                }
                if (disponiveis > 0) {
                    m.setQuantidade(disponiveis);
                } else {
                    m.setDisponivel(false);
                    m.setValor(0.);
                    m.setPrevisaoDisponibilidade(dataPrev);
                }
                return m;
            }).forEachOrdered((m) -> {
                catalogoDTO.getMidias().add(m);                
            });
            catalogo.add(catalogoDTO);
        }
        return new PageImpl<>(catalogo, pageable, catalogo.size());
    }

    
    public Page<Filme> findByTituloOrCategoriaOrLancamento(
                Pageable pageable,
                String titulo, 
                Categoria categoria,
                LocalDate lancamento){
        return filmeRepository
                .findByTituloOrCategoriaOrLancamentoIgnoreCaseContaining(pageable, titulo, categoria, lancamento);
   } 
}
