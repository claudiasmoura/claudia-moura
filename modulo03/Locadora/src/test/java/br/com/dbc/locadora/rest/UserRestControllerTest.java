package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.User;
import br.com.dbc.locadora.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author claudia.moura
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRestControllerTest {

    @Test
    public void contextLoads() {
    }

    private MockMvc restMockMvc;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private UserRestController userRestController;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.restMockMvc = MockMvcBuilders.standaloneSetup(userRestController)
                .setCustomArgumentResolvers(pageableArgumentResolver)
                .setMessageConverters(jacksonMessageConverter).build();
        userRepository.deleteAll();
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testCriarUsuario() throws Exception {
        User u = User.builder().username("usuario").password("senha").firstName("Eu").lastName("Mesmo").build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(u)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(u.getUsername()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(u.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(u.getLastName()));
        List<User> user = userRepository.findAll();
        Assert.assertEquals(1, user.size());        
        Assert.assertEquals(user.get(0).getFirstName(), u.getFirstName());
        Assert.assertEquals(user.get(0).getLastName(), u.getLastName());
        Assert.assertEquals(user.get(0).getUsername(), u.getUsername());   
    }
}