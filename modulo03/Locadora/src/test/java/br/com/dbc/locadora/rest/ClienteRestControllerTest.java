package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.repository.ClienteRepository;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author claudia.moura
 */
public class ClienteRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private ClienteRestController clienteRestController;

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    protected AbstractRestController getController() {
        return clienteRestController;
    }

    @Before
    public void beforeTest() {
        super.setUp();
        Mockito.reset();
        clienteRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteCreateTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome completo").endereco("rua x nro tal").telefone("999999999").build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/cliente")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.endereco").value(c.getEndereco()));
        List<Cliente> clientes = clienteRepository.findAll();
        Assert.assertEquals(1, clientes.size());
        Assert.assertEquals(c.getNome(), clientes.get(0).getNome());
        Assert.assertEquals(c.getEndereco(), clientes.get(0).getEndereco());
        Assert.assertEquals(c.getTelefone(), clientes.get(0).getTelefone());
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteUpdateTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome completo").endereco("endereco tal").telefone("99881166").build();
        c = clienteRepository.save(c);
        
        c.setNome("Pavê de chocolate");
        c.setEndereco("novo Endereço");
        
        restMockMvc.perform(MockMvcRequestBuilders.put("/api/cliente/" + c.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.endereco").value(c.getEndereco()));
        List<Cliente> clientes = clienteRepository.findAll();
    
        Assert.assertEquals(c.getNome(), clientes.get(0).getNome());
        Assert.assertEquals(c.getEndereco(), clientes.get(0).getEndereco());
        Assert.assertEquals(c.getTelefone(), clientes.get(0).getTelefone());
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteDeleteTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome completo").endereco("endereco tal").telefone("99881166").build();
        c = clienteRepository.save(c);
        
        restMockMvc.perform(MockMvcRequestBuilders.delete("/api/cliente/" + c.getId()))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
        
        List<Cliente> clientes = clienteRepository.findAll();
    
        Assert.assertEquals(0, clientes.size());
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteFindByIdTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome completo").endereco("endereco tal").telefone("99881166").build();
        c = clienteRepository.save(c);
                
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/cliente/" + c.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.endereco").value(c.getEndereco()));
        List<Cliente> clientes = clienteRepository.findAll();
    
        Assert.assertEquals(c.getNome(), clientes.get(0).getNome());
        Assert.assertEquals(c.getEndereco(), clientes.get(0).getEndereco());
        Assert.assertEquals(c.getTelefone(), clientes.get(0).getTelefone());
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteFindAllTest() throws Exception {
        Cliente c1 = Cliente.builder().nome("nomeCompleto1").endereco("endereco").telefone("99998888").build();
        Cliente c2 = Cliente.builder().nome("nomeCompleto2").endereco("endereco").telefone("99998888").build();
        Cliente c3 = Cliente.builder().nome("nomeCompleto3").endereco("endereco").telefone("99998888").build();
        c1 = clienteRepository.save(c1);
        c2 = clienteRepository.save(c2);
        c3 = clienteRepository.save(c3);
                
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/cliente/"))
                .andExpect(MockMvcResultMatchers.status().isOk());
              
        List<Cliente> clientes = clienteRepository.findAll();
        Assert.assertEquals(3, clientes.size());
        Assert.assertEquals(c1.getNome(), clientes.get(0).getNome());
        Assert.assertEquals(c2.getNome(), clientes.get(1).getNome());
        Assert.assertEquals(c3.getNome(), clientes.get(2).getNome());
    }        
}
