package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.enums.Categoria;
import br.com.dbc.locadora.enums.MidiaType;
import br.com.dbc.locadora.repository.FilmeRepository;
import br.com.dbc.locadora.repository.MidiaRepository;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import br.com.dbc.locadora.service.FilmeService;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import static java.time.format.DateTimeFormatter.ofPattern;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


/**
 *
 * @author claudia.moura
 */
public class FilmeRestControllerTest extends LocadoraApplicationTests {
    
    @Autowired
    private FilmeRestController filmeRestController;

    @Autowired
    private FilmeRepository filmeRepository;
    
    @Autowired
    private MidiaRepository midiaRepository;
    
    @Autowired
    private ValorMidiaRepository valorMidiaRepository;
    
    @Autowired
    private FilmeService filmeService;

    protected AbstractRestController getController() {
        return filmeRestController;
    }

    @Before
    public void beforeTest() {
        super.setUp();
        Mockito.reset();
        valorMidiaRepository.deleteAll();
        midiaRepository.deleteAll();
        filmeRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getService method, of class FilmeRestController.
     */

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testSalvarComMidia() throws Exception {
        
        List<MidiaDTO> midiasDTO = Arrays.asList(
                        MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(1).valor(1.5).build(),
                        MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(1).valor(2.5).build(),
                        MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(1).valor(5.).build());
        
        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("A Hora do Pesadelo")
                .lancamento(LocalDate.of(2014, Month.JANUARY, 1))
                .categoria(Categoria.ACAO)
                .midia(midiasDTO)
                .build();
        
        Filme filme = filmeDTO.transformaParaFilme();        
        
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia/")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsBytes(filmeDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filmeDTO.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(filmeDTO.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(filme.getCategoria().toString()));
        
        List<Filme> filmes = filmeRepository.findAll();
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(filme.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(filme.getLancamento(), filmes.get(0).getLancamento());
        Assert.assertEquals(filme.getCategoria(), filmes.get(0).getCategoria());
        
        List<Midia> midias = midiaRepository.findAll();
        Assert.assertEquals(3, midias.size());
        Assert.assertEquals(midiasDTO.get(0).getTipo(), midias.get(0).getTipo());
        Assert.assertEquals(filme.getTitulo(), midias.get(0).getFilme().getTitulo());
        
        List<ValorMidia> valorMidias = valorMidiaRepository.findAll();
        Assert.assertEquals(3, valorMidias.size());
        Assert.assertEquals(midias.get(0), valorMidias.get(0).getMidia());
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testAtualizarComMidia() throws Exception {
        List<MidiaDTO> midiasDTO = Arrays.asList(
                        MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(5).valor(1.5).build(),
                        MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(5).valor(2.5).build(),
                        MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(5).valor(5.).build());
        
        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("A Hora do Pesadelo")
                .lancamento(LocalDate.of(2014, Month.JANUARY, 1))
                .categoria(Categoria.ACAO)
                .midia(midiasDTO)
                .build();
        
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia/")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsBytes(filmeDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk());
        
        Filme filme = filmeRepository.findAll().get(0);
        
        List<MidiaDTO> midiasDTO2 = Arrays.asList(
                MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(2).valor(1.5).build(),
                MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(10).valor(3.5).build(),
                MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(5).valor(4.).build());
        
        filmeDTO.setId(filme.getId());
        filmeDTO.setLancamento(LocalDate.of(1988, Month.JANUARY, 1));
        filmeDTO.setCategoria(Categoria.AVENTURA);
        filmeDTO.setMidia(midiasDTO2);

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/filme/" + filme.getId() + "/midia/")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsBytes(filmeDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filmeDTO.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(filmeDTO.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(filmeDTO.getCategoria().toString()));
        
        Filme filmeAtual = filmeRepository.findAll().get(0);
        List<Midia> midias = midiaRepository.findAll();
        
        Assert.assertEquals(filmeDTO.getCategoria(), filmeAtual.getCategoria());
        Assert.assertEquals(filmeDTO.getLancamento(), filmeAtual.getLancamento());
        Assert.assertEquals(filmeDTO.getTitulo(), filmeAtual.getTitulo());
        Assert.assertEquals(17, midias.size());       
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testBuscaCatalogo() throws Exception {
        List<MidiaDTO> midiasDTO = Arrays.asList(
                        MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(1).valor(1.5).build(),
                        MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(1).valor(2.5).build(),
                        MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(1).valor(5.).build());
        
        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("A Hora do Pesadelo")
                .lancamento(LocalDate.of(1988, Month.JANUARY, 1))
                .categoria(Categoria.ACAO)
                .midia(midiasDTO)
                .build();
        
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia/")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsBytes(filmeDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk());
        filmeService.salvarComMidia(filmeDTO);
        
        FilmeDTO filmeTitulo = FilmeDTO.builder().titulo("hora").build();
        
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/search/catalogo")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsBytes(filmeTitulo)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].titulo").value("A Hora do Pesadelo"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].categoria").value(Categoria.ACAO.toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].lancamento").value(LocalDate.of(1988, Month.JANUARY, 1).format(ofPattern("dd/MM/yyyy")) ))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[0].disponivel").value(true))                
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[1].disponivel").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[2].disponivel").value(true)); 
    }
    
}
