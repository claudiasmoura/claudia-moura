package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import static br.com.dbc.locadora.enums.Categoria.ACAO;
import br.com.dbc.locadora.enums.MidiaType;
import br.com.dbc.locadora.repository.AluguelRepository;
import br.com.dbc.locadora.repository.ClienteRepository;
import br.com.dbc.locadora.repository.FilmeRepository;
import br.com.dbc.locadora.repository.MidiaRepository;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import br.com.dbc.locadora.service.FilmeService;
import java.time.LocalDate;
import java.time.LocalDateTime;
import static java.time.format.DateTimeFormatter.ofPattern;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author claudia.moura
 */
public class AluguelRestControllerTest extends LocadoraApplicationTests {
    
    public AluguelRestControllerTest() {
    }
    
    @Autowired
    private AluguelRestController aluguelRestController;

    @Autowired
    private AluguelRepository aluguelRepository;
    
    @Autowired
    private ValorMidiaRepository valorMidiaRepository;
    
    @Autowired
    private ClienteRepository clienteRepository;
    
    @Autowired
    private MidiaRepository midiaRepository;
    
    @Autowired
    private FilmeRepository filmeRepository;
    
    @Autowired
    private FilmeService filmeService;
    
    protected AbstractRestController getController() {
        return aluguelRestController;
    }

    @Before
    public void beforeTest() {
        super.setUp();
        Mockito.reset();
        valorMidiaRepository.deleteAll();
        midiaRepository.deleteAll();
        filmeRepository.deleteAll();
        aluguelRepository.deleteAll();
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testSalvarAluguel() throws Exception {
        Cliente cliente = Cliente.builder().nome("Freddy Krueger").endereco("Elm Street").telefone("666 333 333").build();
        cliente = clienteRepository.save(cliente);
        
        List<MidiaDTO> midiasDTO = Arrays.asList(
                MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(1).valor(1.5).build(),
                MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(1).valor(2.5).build(),
                MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(1).valor(5.).build());

        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("A Hora do Pesadelo")
                .lancamento(LocalDate.now())
                .categoria(ACAO)
                .midia(midiasDTO)
                .build();
    
        Filme filme = filmeService.salvarComMidia(filmeDTO);       
        List<Midia> midias = midiaRepository.findByFilme(filme);
        
        List<Long> midiasId = Arrays.asList(
                midias.get(0).getId(),
                midias.get(1).getId()
        );
        
        AluguelDTO aluguelDTO = AluguelDTO.builder()
                .id(1l)
                .idCliente(cliente.getId())
                .midias(midiasId)
                .retirada(LocalDateTime.now())
                .build();
        
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/retirada/")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsBytes(aluguelDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.retirada").value(LocalDate.now().atStartOfDay()
                        .format(ofPattern("dd/MM/yyyy HH:mm"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.previsao").value(LocalDate.now().plusDays(aluguelDTO.getMidias()
                        .size()).atStartOfDay().plusHours(16).format(ofPattern("dd/MM/yyyy HH:mm"))));
    }

    /**
     * Test of devolverMidia method, of class AluguelRestController.
     */
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testDevolverMidia() throws Exception {
        Cliente cliente = Cliente.builder().nome("Freddy Krueger").endereco("Elm Street").telefone("666 333 333").build();
        cliente = clienteRepository.save(cliente);
        
        List<MidiaDTO> midiasDTO = Arrays.asList(
                MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(1).valor(1.5).build(),
                MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(1).valor(2.5).build(),
                MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(1).valor(5.).build());

        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("A Hora do Pesadelo")
                .lancamento(LocalDate.now())
                .categoria(ACAO)
                .midia(midiasDTO)
                .build();
    
        Filme filme = filmeService.salvarComMidia(filmeDTO);     
        
        List<Midia> midias = midiaRepository.findAll();
        
        AluguelDTO retirada = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(Arrays.asList(
                        midias.get(0).getId(),
                        midias.get(1).getId(),
                        midias.get(2).getId()))
                .build();
        
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/retirada")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(retirada)))
                .andExpect(MockMvcResultMatchers.status().isOk());
        
        AluguelDTO devolucao = AluguelDTO.builder()
                .midias(Arrays.asList(
                        midias.get(0).getId(),
                        midias.get(1).getId(),
                        midias.get(2).getId()))
                .build();
        
        List<Aluguel> alugueis = aluguelRepository.findAll();
        
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/devolucao")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(devolucao)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.retirada").value(alugueis.get(0).getRetirada().format(ofPattern("dd/MM/yyyy HH:mm"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.previsao").value(alugueis.get(0).getPrevisao().format(ofPattern("dd/MM/yyyy HH:mm"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.devolucao").value(LocalDateTime.now().format(ofPattern("dd/MM/yyyy HH:mm"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.valor").value(9))
                .andExpect(MockMvcResultMatchers.jsonPath("$.multa").value(0.));
    }

    /**
     * Test of devolucaoAluguelHoje method, of class AluguelRestController.
     */
    //@Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testDevolucaoAluguelHoje() {
        System.out.println("devolucaoAluguelHoje");
        Pageable pageable = null;
        AluguelRestController instance = new AluguelRestController();
        ResponseEntity<Page<Filme>> expResult = null;
        ResponseEntity<Page<Filme>> result = instance.devolucaoAluguelHoje(pageable);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}
