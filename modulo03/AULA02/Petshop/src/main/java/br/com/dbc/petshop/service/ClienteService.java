/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.service;

import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.HibernateUtil;
import br.com.dbc.petshop.entity.PersistenceUtils;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author claudia.moura
 */
public class ClienteService {
    EntityManager em = PersistenceUtils.getEm();
    
    private static ClienteService instance;
    public static ClienteService getInstance() {
        return instance == null ? new ClienteService() : instance;
    }

    public ClienteService() {
    }
    
    public List<Cliente> buscarClientePorId(Long id) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            return session.createCriteria(Cliente.class)
                    .add(Restrictions.ilike("id", id))
                    .list();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public List<Cliente> buscarClientePorNomeCriteria(String nome) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            return session.createCriteria(Cliente.class)
                    .add(Restrictions.ilike("nome", nome))
                    .list();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
