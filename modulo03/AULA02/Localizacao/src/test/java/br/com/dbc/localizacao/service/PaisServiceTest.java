/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.localizacao.service;

import br.com.dbc.localizacao.entity.HibernateUtil;
import br.com.dbc.localizacao.entity.Pais;
import br.com.dbc.localizacao.entity.PersistenceUtils;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author tiago
 */
public class PaisServiceTest {
    
    public PaisServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        PersistenceUtils.getEm();
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        Pais p = (Pais)session.merge(new Pais("Brazil"));
        t.commit();
        session.close();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testBuscarPaisPorNome() {
        System.out.println("buscarPaisPorNome");
        String nome = "%razi%";
        PaisService instance = PaisService.getInstance();
        String expNome = "Brazil";
        List<Pais> result = instance.buscarPaisPorNome(nome);
        assertEquals(1, result.size());
        assertEquals(expNome, result.get(0).getNome());
    }
    
    @Test
    public void testBuscarPaisPorNomeCriteria() {
        System.out.println("testBuscarPaisPorNomeCriteria");
        String nome = "%razi%";
        PaisService instance = PaisService.getInstance();
        String expNome = "Brazil";
        List<Pais> result = instance.buscarPaisPorNomeCriteria(nome);
        assertEquals(1, result.size());
        assertEquals(expNome, result.get(0).getNome());
    }
    
}
