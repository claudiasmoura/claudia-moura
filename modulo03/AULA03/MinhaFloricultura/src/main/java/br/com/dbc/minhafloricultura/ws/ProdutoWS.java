package br.com.dbc.minhafloricultura.ws;

import br.com.dbc.minhafloricultura.dao.AbstractDAO;
import br.com.dbc.minhafloricultura.dao.ProdutoDAO;
import br.com.dbc.minhafloricultura.entity.Produto;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.persistence.EntityManager;

/**
 *
 * @author claudia.moura
 */
@Stateless
@WebService(serviceName = "ProdutoWS")
public class ProdutoWS extends AbstractCrudWs<ProdutoDAO, Produto> {

    @EJB
    private ProdutoDAO produtoDAO;

    @Override
    @WebMethod(exclude = true)
    public ProdutoDAO getDAO() {
        return produtoDAO;
    }
}
