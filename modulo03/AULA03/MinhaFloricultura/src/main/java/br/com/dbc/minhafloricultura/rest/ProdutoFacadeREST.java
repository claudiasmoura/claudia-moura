package br.com.dbc.minhafloricultura.rest;

import br.com.dbc.minhafloricultura.dao.ProdutoDAO;
import br.com.dbc.minhafloricultura.entity.Produto;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author claudia.moura
 */
@Stateless
@Path("produto")
public class ProdutoFacadeREST extends AbstractFacade<Produto, ProdutoDAO> {
    
    @Inject
    private ProdutoDAO produtoDAO;
    
    @Override
    protected ProdutoDAO getDAO() {
    return produtoDAO;
    }
    
    @GET
    @Path("descricao/{descricao}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response find(@PathParam("descricao") String descricao) {
        return Response.ok(getDAO().findDescricao(descricao)).build();
    }
}
