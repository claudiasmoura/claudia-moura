package br.com.dbc.minhafloricultura.dao;

import br.com.dbc.minhafloricultura.entity.Venda;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author claudia.moura
 */
@Stateless
public class VendaDAO extends AbstractDAO<Venda> {

    @PersistenceContext(unitName = "minha_floricultura_pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VendaDAO() {
        super(Venda.class);
    }
    
}
