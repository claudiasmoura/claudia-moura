package br.com.dbc.minhaSeguradora.service;

import br.com.dbc.minhaSeguradora.entity.Apolice;
import br.com.dbc.minhaSeguradora.repository.ApoliceRepository;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author claudia.moura
 */
@Service
public class ApoliceService extends AbstractCrudService<Apolice> {
    
    @Autowired
    private ApoliceRepository apoliceRepository;    
    
    @Override
    protected JpaRepository<Apolice, Long> getRepository() {
        return apoliceRepository;
    }
    
//    public Apolice update(Long id, Apolice apolice) {
//        apolice.setId(id);
//        apoliceRepository.save(apolice);
//        return apolice;
//    }    
}
