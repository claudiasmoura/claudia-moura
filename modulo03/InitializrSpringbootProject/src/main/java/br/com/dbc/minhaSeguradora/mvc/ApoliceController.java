/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.minhaSeguradora.mvc;

import br.com.dbc.minhaSeguradora.entity.Apolice;
import br.com.dbc.minhaSeguradora.service.ApoliceService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author claudia.moura
 */
@Controller
public class ApoliceController {

    @Autowired
    private ApoliceService apoliceService;

    @GetMapping("/")
    public String findAll(Model model) {
        List<Apolice> apolices = apoliceService.findAll(PageRequest.of(0, 20)).getContent();
        model.addAttribute("apolices", apolices);
        return "apolice";
    }

    @PostMapping("/")
    public String save(Apolice apolice, Model model) {
        List<Apolice> apolices = apoliceService.findAll(PageRequest.of(0, 20)).getContent();
        model.addAttribute("apolices", apolices);
        return "apolice";
    }
}
