
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshophibernate.service;

import br.com.dbc.petshophibernate.dao.ClienteDAO;
import br.com.dbc.petshophibernate.dao.HibernateUtil;
import br.com.dbc.petshophibernate.entity.Animal;
import br.com.dbc.petshophibernate.entity.Cliente;
import java.util.Arrays;
import java.util.List;
import javassist.NotFoundException;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 *
 * @author claudia.moura
 */
public class ClienteServiceTest {

    public ClienteServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCreateMocked() {
        System.out.println("create mocked");
        Cliente cliente1 = Cliente.builder()
                .nome("armando")
                .animalList(
                        Arrays
                        .asList(Animal
                                .builder()
                                .nome("dog")
                                .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(cliente1);
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        clienteService.create(cliente1);
        Mockito.verify(daoMock, times(1)).createOrUpdate(cliente1);
    }
    
    @Test(expected = IllegalArgumentException.class) 
    public void testCreateException() {
        ClienteService.getInstance().create(Cliente.builder().id(1l).build());
    }
    
    @Test
    public void testUpdateMocked() {
        System.out.println("update mocked");
        Cliente cliente1 = Cliente.builder()
                .id(1l)
                .nome("armando")
                .animalList(
                        Arrays
                        .asList(Animal
                                .builder()
                                .nome("dog")
                                .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class); //cria objeto que simula ClienteDAO
        Mockito.doNothing().when(daoMock).createOrUpdate(cliente1); //define o que o mock faz no metodo createOrUpdate
        ClienteService clienteService = Mockito.spy(ClienteService.class); //cria objeto que usa metodos reais de clienteService
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock); //define o que retornar quando utilizar metodo getDAO
        clienteService.update(cliente1); // teste do método update da classe ClienteService
        Mockito.verify(daoMock, times(1)).createOrUpdate(cliente1); //confere se createOrUpdate chama metodo do DAO corretamente
    }
    
    @Test
    public void testDeleteMocked() throws NotFoundException {
        System.out.println("delete mocked");
        Cliente cliente1 = Cliente.builder()
                .id(1l)
                .nome("armando")
                .animalList(
                        Arrays
                        .asList(Animal
                                .builder()
                                .nome("dog")
                                .build()))
                .build();
        
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).delete(cliente1);//define que metodo delete não faz nada
        Mockito.doReturn(cliente1).when(daoMock).findById(cliente1.getId()); //define que metodo findById retorna cliente1
        
        ClienteService clienteService = Mockito.spy(ClienteService.class);        
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        
        clienteService.delete(cliente1.getId());
        Mockito.verify(daoMock, times(1)).findById(1l); // verifica se findById é chamado corretamente
        Mockito.verify(daoMock, times(1)).delete(cliente1);//verifica se delete é chamado corretamente
    }
    
    
    
    //teste sem usar dados mockados
    //@Test
    public void testCreate() {
        System.out.println("create");
        Cliente cliente1 = Cliente.builder()
                .nome("armando")
                .animalList(
                        Arrays
                        .asList(Animal
                                .builder()
                                .nome("dog")
                                .build()))
                .build();
        ClienteService.getInstance().create(cliente1);
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Cliente> clientes = session.createCriteria(Cliente.class).list();
        Assert.assertEquals("Quantidade de clientes errada", 1, clientes.size());
        Cliente result = clientes.stream().findAny().get();
        Assert.assertEquals("Cliente diferente", cliente1.getId(), result.getId());
        Assert.assertEquals("Animais Diferentes", cliente1.getAnimalList().stream().findAny().get().getId(),
                result.getAnimalList().stream().findAny().get().getId());

        session.close();
    }
    
    public void testFindAll() {
        
    
    }
}
