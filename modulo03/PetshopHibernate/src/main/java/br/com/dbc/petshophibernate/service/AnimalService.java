package br.com.dbc.petshophibernate.service;

import br.com.dbc.petshophibernate.dao.AnimalDAO;
import br.com.dbc.petshophibernate.entity.Animal;

/**
 *
 * @author claudia.moura
 */
public class AnimalService extends AbstractCRUDService {
    private static AnimalService instance;

    static {
        instance = new AnimalService();
    }

    public static AnimalService getInstance() {
        return instance;
    }
    
    @Override
    protected AnimalDAO getDAO() {
        return AnimalDAO.getInstance();
    }
}
