public class Numeros
{
    double listaNumeros[];

    public Numeros (double listaNumeros[]) {
        this.listaNumeros = listaNumeros;
    }

    public double [] calcularMediaSeguinte() {
        /*
         * retorna um array com a média entre cada número e o número seguinte
         */
        double[] resultado = new double[listaNumeros.length - 1];
        
        for (int i = 0; i < listaNumeros.length - 1; i++) {
            double media = (listaNumeros[i] + listaNumeros[i + 1]) / 2;
            resultado[i] = media;
        } 
        
        return resultado;
    }
}
