public class Programa {
    public static void main(String[] args) {
        int entrada = 1;
        int x = 5;
        int y = 10;
        Retangulo formaGeometrica = FormaGeometricaFactory.criar(entrada, x, y);
        System.out.println(formaGeometrica.calcularArea());
    }
}