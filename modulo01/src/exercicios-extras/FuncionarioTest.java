import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FuncionarioTest
{
    @Test
    public void calculaSalarioFinal(){
        assertEquals(450, new Funcionario("Capitão América", 500.0, 1000.0).salarioFinal(), 0.01);
    }

    @Test
    public void calculaComissaoFuncionario(){
        assertEquals(135, new Funcionario("Capitão América", 500.0, 1000.0).totalComissao(), 0.01);
    }
    
    @Test 
    public void CalculaComissaoFuncionario2(){
        assertEquals(1058.4675, new Funcionario("Rogue", 500.0, 7840.50).totalComissao(), 0.01);
    }

    @Test
    public void calculaLucroFuncionario(){
        assertEquals( 585.0, new Funcionario("Capitão América", 500.0, 1000.0).getLucro(), 0.01);
    }

    @Test
    public void calculaLucroFuncionario2(){
        assertEquals(1508.4675, new Funcionario("Rogue", 500.0, 7840.50).getLucro(), 1);
    }
}