import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NumeroTest
{
    @Test
    public void numeroImparRetornaTrue(){
        assertTrue(new Numero(7).impar());
    }

    @Test
    public void numeroParRetornaFalse(){
        assertFalse(new Numero(8).impar());
    }

    @Test
    public void somaDosDigitosDivisivel(){
        assertTrue(new Numero(9).verificarSomaDivisivel(1892376));
    }

    @Test
    public void somaDosDigitosNaoDivisivel(){
        assertFalse(new Numero(3).verificarSomaDivisivel(17));
    }

    @Test
    public void somaDosDigitosIgualAZero(){
        assertTrue(new Numero(3).verificarSomaDivisivel(0));
    }

}