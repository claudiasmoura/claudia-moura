public class FormaGeometricaFactory {
    public static Retangulo criar(int tipo, int x, int y) {
        Retangulo formaGeometrica = null;
        switch (tipo) {
            case 1:
                formaGeometrica = new Retangulo();
            case 2:
                formaGeometrica = new Quadrado(); 
        }
        formaGeometrica.setX(x);
        formaGeometrica.setY(y);
        
        return formaGeometrica;
    }
}