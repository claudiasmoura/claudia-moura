import java.util.*;

public class AgendaContatos 
{
    private LinkedHashMap<String, String> agendaContatos;

    public AgendaContatos() {
        this.agendaContatos = new LinkedHashMap<>();
    }

    public void adicionarContato(String nome, String telefone) {
        agendaContatos.put(nome, telefone);
    }

    public String buscaContatoPorNome(String nome) {
        return agendaContatos.get(nome);
    }

    public String buscaContatoPorNumero(String numero) {
        String nome = "";
        for (Map.Entry<String,String> contato : agendaContatos.entrySet()) {
            if (contato.getValue().contains(numero)) {
                nome = contato.getKey();
            }
        }
        return nome;
    }

    public String csv(){
        StringBuilder contatos = new StringBuilder();
        String separador = System.lineSeparator();

        for (Map.Entry<String,String> contato : agendaContatos.entrySet()) {
            String chave = contato.getKey();
            String valor = contato.getValue();
            String contatoFormatado = String.format("%s,%s%s", chave, valor, separador);
            contatos.append(contatoFormatado);
        }
        return contatos.toString();
    }
}
