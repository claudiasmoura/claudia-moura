import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NumerosTest
{
    @Test
    public void retornaListaComMediasEntreNumeros(){
        double[] entrada = new double[] {1.0, 3.0, 5.0, 1.0, -10.0};
        Numeros numeros = new Numeros(entrada);
        assertArrayEquals(new double[]{2.0, 4.0, 3.0, -4.5}, numeros.calcularMediaSeguinte(), 0.1);
    }
    
    @Test
    public void retornaListaComMediasEntreNumeros2(){
        double[] entrada = new double[] {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
        Numeros numeros = new Numeros(entrada);
        assertArrayEquals(new double[]{1.5, 2.5, 3.5, 4.5, 5.5}, numeros.calcularMediaSeguinte(), 0.1);
    }
}
