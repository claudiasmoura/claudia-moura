import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest
{
    @Test
    public void adicionaContatoNaHashMap() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Pedro", "5551");

        assertTrue("5551".equals(agenda.buscaContatoPorNome("Pedro")));
    }

    @Test
    public void buscaContatoPorNomeRetornaContatoCorreto() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Pedro", "5551");    
        agenda.adicionarContato("Paulo", "5552");
        agenda.adicionarContato("Mateus", "5553");

        assertTrue("5552".equals(agenda.buscaContatoPorNome("Paulo")));
    }

    @Test
    public void buscaContatoPorNumeroRetornaNomeCorreto() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Pedro", "5551");    
        agenda.adicionarContato("Paulo", "5552");
        agenda.adicionarContato("Mateus", "5553");

        String resultado = agenda.buscaContatoPorNome("Mateus"); 

        assertEquals("5553", resultado);
    }

    @Test
    public void csvRetornaContatosEmString() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Pedro", "5551");    
        agenda.adicionarContato("Paulo", "5552");
        agenda.adicionarContato("Mateus", "5553");

        String separador = System.lineSeparator();
        String esperado = String.format("Pedro,5551%sPaulo,5552%sMateus,5553%s", separador, separador, separador);
        String b = agenda.csv();
        assertEquals(esperado, agenda.csv());
    }
}
