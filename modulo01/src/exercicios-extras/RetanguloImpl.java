public class RetanguloImpl implements FormaGeometrica {
    private int x, y;

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y){
        this.y = y;
    }
    
    public int calcularArea(){
        return x * y;
    };
}