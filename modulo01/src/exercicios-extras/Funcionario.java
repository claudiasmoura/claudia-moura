public class Funcionario {
    String nome;
    double salarioFixo, totalVendas;
    
    public Funcionario(String nome, double salario, double totalVendas) {
        this.nome = nome;
        this.salarioFixo = salario;
        this.totalVendas = totalVendas;
    }
    
    double salarioFinal(){
        return this.salarioFixo - this.salarioFixo * 0.1;
    }
    
    double totalComissao(){
        double comissao = this.totalVendas * 0.15;
        return  comissao - comissao * 0.1;
    }
    
    double getLucro(){
        return this.salarioFinal() + this.totalComissao();
    }    
}
