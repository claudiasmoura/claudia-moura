public class Numero{
    int numero;

    public Numero(int numero){
        this.numero = numero;
    }

    public boolean impar(){
        return this.numero % 2 == 1;
    }
    
    public boolean verificarSomaDivisivel(int numeroSoma){
        if (numeroSoma == 0) return true;
        
        int soma = 0;

        while (numeroSoma > 0) {
            soma += numeroSoma % 10;
            numeroSoma /= 10;
        }        
  
        return soma % this.numero == 0;
    }
    
}