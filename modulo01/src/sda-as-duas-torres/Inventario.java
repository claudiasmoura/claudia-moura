import java.util.ArrayList;
import java.util.Arrays;

public class Inventario
{
    ArrayList<Item> inventario;

    public Inventario() {
        this.inventario = new ArrayList<Item>();
    }

    public void adicionar(Item novoItem) {
        this.inventario.add(novoItem);
    }

    public ArrayList<Item> getItens() {
        return this.inventario;
    }

    public Item obter(int posicao) {
        return this.inventario.get(posicao);
    }

    public void remover(int posicao) {
        this.inventario.remove(posicao);
    }

    public void remover(Item item) {
        this.inventario.remove(item);
    }

    public String getDescricoesItens() {
        if (this.inventario.isEmpty()) return null;

        StringBuilder descricoes = new StringBuilder(1024);
        for (Item item: inventario) {
            descricoes.append(item.getDescricao());
            descricoes.append(",");
        }

        return descricoes.substring(0, descricoes.length() - 1).toString();
    }

    public Item maiorQuantidade() {
        if (this.inventario.isEmpty()) return null;

        int maiorQuantidade = 0, posicao = 0;
        for (Item item: inventario) {
            if (item.getQuantidade() > maiorQuantidade) {
                maiorQuantidade = item.getQuantidade();
                posicao = inventario.indexOf(item);
            }
        }
        return inventario.get(posicao);
    }

    public Item buscar(String descricao) {
        if (this.inventario.isEmpty()) return null;

        for (Item item: inventario) {
            if (item.getDescricao().equals(descricao)) {
                return item;
            }            
        }
        return null;
    }

    public ArrayList<Item> inverter() {
        if (this.inventario.isEmpty()) return null;

        ArrayList<Item> inventarioInvertido = new ArrayList<>();
        for (int i = inventario.size() -1; i >= 0; i--) {
            inventarioInvertido.add(this.inventario.get(i));
        }

        return inventarioInvertido;
    }

    public void ordenarItem() {
        if (this.inventario.isEmpty()) return;

        boolean listaForaDeOrdem = true;
        int posicaoInicial = 0;
        Item temp;        
        Item[] arrayOrdenado = new Item[this.inventario.size()];

        for (int i = 0; i < this.inventario.size(); i++) {
            arrayOrdenado[i] = this.inventario.get(i);
        }        

        while (listaForaDeOrdem) {            
            listaForaDeOrdem = false;
            for (int i = posicaoInicial; i < arrayOrdenado.length - 1; i++) {
                if (arrayOrdenado[i].quantidade > arrayOrdenado[i + 1].quantidade) {
                    temp = arrayOrdenado[i];
                    arrayOrdenado[i] = arrayOrdenado[i + 1];
                    arrayOrdenado[i + 1] = temp;                    
                    if (!listaForaDeOrdem && i > 0) {
                        posicaoInicial = i - 1;
                        listaForaDeOrdem = true;
                    }
                }
            }
        }
        this.inventario = new ArrayList<Item>(Arrays.asList(arrayOrdenado));

    }

    public void ordenarItem(TipoOrdenacao ordenacao) {
        if (this.inventario.isEmpty()) return;

        this.ordenarItem();
        ArrayList<Item> inventarioOrdenadoDesc = new ArrayList<>();

        if (ordenacao == TipoOrdenacao.DESC) {
            for (int i = this.inventario.size() -1; i >= 0; i--) {
                inventarioOrdenadoDesc.add(this.inventario.get(i));
            }            
            this.inventario = inventarioOrdenadoDesc;
        }        
    }

    public Inventario unir(Inventario inventario) {
        Inventario inventarioUnir = new Inventario();
        inventarioUnir.getItens().addAll(this.inventario);
        inventarioUnir.getItens().addAll(inventario.getItens());
        return inventarioUnir;
    }

    public Inventario diferenciar(Inventario inventario) {
        Inventario diferentes = new Inventario();
        diferentes.getItens().addAll(this.inventario);
        diferentes.getItens().removeAll(inventario.getItens());
        return diferentes;
    }

    public Inventario cruzar(Inventario inventario) {
        Inventario cruzado = new Inventario();
        for (Item item: this.inventario) {
            if (inventario.getItens().contains(item)) {
                cruzado.adicionar(item);
            }
        }
        return cruzado;
    }
}