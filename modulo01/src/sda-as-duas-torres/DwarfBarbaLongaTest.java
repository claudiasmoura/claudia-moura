import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfBarbaLongaTest
{
    @Test
    public void sorteioComNumeroAcimaDe4NaoTiraVida() {
        Dwarf dudu = new DwarfBarbaLonga("Dwdw", new DadoFake(5));
        
        dudu.sofreDano();
        
        assertEquals(110, dudu.getVida(), 0.01);        
    }
    
    @Test
    public void sorteioComNumeroAbaixoDe4TiraVida() {
        Dwarf dudu = new DwarfBarbaLonga("Dwdw", new DadoFake(4));
        
        dudu.sofreDano();
        
        assertEquals(100, dudu.getVida(), 0.01);        
    }
    
    @Test
    public void sorteioComNumeroIgualA4TiraVida() {
        Dwarf dudu = new DwarfBarbaLonga("Dwdw", new DadoFake(2));
        
        dudu.sofreDano();
        
        assertEquals(100, dudu.getVida(), 0.01);        
    }
}
