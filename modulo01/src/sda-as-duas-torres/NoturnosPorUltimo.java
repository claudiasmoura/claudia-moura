import java.util.*;

public class NoturnosPorUltimo implements Estrategia {
    HashMap<Class, ArrayList<Elfo>> porClasse = new HashMap<>();

    public List<Elfo> getOrdemDeAtaque(List<Elfo> atacantes) {
        for (Elfo elfo : atacantes) {
            if (elfo.getStatus() == Status.VIVO) {
                this.inicializarListaDeClasse(elfo.getClass());
                porClasse.get(elfo.getClass()).add(elfo);
            }
        }
        ArrayList<Elfo> ordenados = new ArrayList<>();
        
        if (!listaPorClasseVazia(ElfoVerde.class)) ordenados.addAll(porClasse.get(ElfoVerde.class)); 
        if (!listaPorClasseVazia(ElfoDaLuz.class)) ordenados.addAll(porClasse.get(ElfoDaLuz.class)); 
        if (!listaPorClasseVazia(ElfoNoturno.class)) ordenados.addAll(porClasse.get(ElfoNoturno.class));
        
        return ordenados;
    }

    private void inicializarListaDeClasse(Class classe) {
        if (listaPorClasseVazia(classe)) {
            ArrayList<Elfo> elfosPorClasse = new ArrayList<>();
            porClasse.put(classe, elfosPorClasse);
        }
    }
    
    private boolean listaPorClasseVazia(Class classe) {
        return porClasse.get(classe) == null;
    }

}