import static org.junit.Assert.*;
import java.util.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest
{
    @After
    public void tearDown() {
        System.gc();
    }
    
    @Test
    public void elfoDaLuzNasceComArcoFlechaEEspadaDeGalvorn() {
        ElfoDaLuz amanda = new ElfoDaLuz("Amandita");
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item("Arco", 1),
                    new Item("Flecha", 7),
                    new Item("Espada de Galvorn", 1)
                ));

        assertEquals(esperado, amanda.inventario.getItens());
    } 

    @Test 
    public void ataqueEmNumeroImparComEspadaTira21DeVida() {
        ElfoDaLuz amanda = new ElfoDaLuz("Amandita");
        Dwarf diovane = new Dwarf("Didi");
        amanda.atacarComEspada(diovane);

        assertEquals(79, amanda.getVida(), 0.1);
    }

    @Test 
    public void ataqueEmNumeroParComEspadaAcrescenta10DeVida() {
        ElfoDaLuz amanda = new ElfoDaLuz("Amandita");
        Dwarf diovane = new Dwarf("Didi");
        amanda.atacarComEspada(diovane);
        amanda.atacarComEspada(diovane);

        assertEquals(89, amanda.getVida(), 0.1);
    }

    @Test
    public void elfoDaLuzMorreAoChegarA0DeVida() {
        ElfoDaLuz amanda = new ElfoDaLuz("Amandita");
        Dwarf laura = new Dwarf("Lauras");

        for (int i = 0; i < 20; i++) {
            amanda.atacarComEspada(laura);
        }

        assertEquals(0, amanda.getVida(), 0.1);
        assertEquals(Status.MORTO, amanda.getStatus());
    }

    @Test
    public void elfoDaLuzMortoNaoAtaca() {
        ElfoDaLuz amanda = new ElfoDaLuz("Amandita");
        Dwarf laura = new Dwarf("Lauras");

        for (int i = 0; i < 20; i++) {
            amanda.atacarComEspada(laura);
        }

        Dwarf ismael = new Dwarf("Isma");
        amanda.atacarComEspada(ismael);

        assertEquals(110, ismael.getVida(), 0.1);
    }
}
