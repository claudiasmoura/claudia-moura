import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest
{
    @After
    public void tearDown() {
        System.gc();
    }

    @Test
    public void atirar4FlechasAumentaExperienciaEm8() {
        Elfo ghilherme = new ElfoVerde("GG");
        Dwarf renan = new Dwarf("Andrei");

        ghilherme.atirarFlecha(renan);
        ghilherme.atirarFlecha(renan);
        ghilherme.atirarFlecha(renan);
        ghilherme.atirarFlecha(renan);

        assertEquals(8, ghilherme.getExperiencia());
    }

    @Test
    public void elfoVerdeGanhaItensPermitidos() {
        ElfoVerde ghilherme = new ElfoVerde("GG");

        ghilherme.ganharItem(new Item("Flecha de Vidro", 1));        
        ghilherme.ganharItem(new Item("Arco de Vidro", 1));        
        ghilherme.ganharItem(new Item("Flecha", 1));    
        ghilherme.ganharItem(new Item("Escudo", 1));        
        ghilherme.ganharItem(new Item("Espada de aço valiriano", 1));
        ghilherme.ganharItem(new Item("Varinha mágica", 1));
        ghilherme.ganharItem(new Item("Capa da invisibilidade", 1));

        assertEquals(5, ghilherme.inventario.getItens().size());
    }

    @Test

    public void elfoVerdeNaoGanhaItensNaoPermitidos() {
        ElfoVerde ghilherme = new ElfoVerde("GG");

        ghilherme.ganharItem(new Item("Flecha de Vidro", 1));        
        ghilherme.ganharItem(new Item("Arco de Vidro", 1));        
        ghilherme.ganharItem(new Item("Flecha", 1));    
        ghilherme.ganharItem(new Item("Escudo", 1));        
        ghilherme.ganharItem(new Item("Espada de aço valiriano", 1));
        ghilherme.ganharItem(new Item("Varinha mágica", 1));
        ghilherme.ganharItem(new Item("Capa da invisibilidade", 1));

        assertEquals(5, ghilherme.inventario.getItens().size());
    }
}
