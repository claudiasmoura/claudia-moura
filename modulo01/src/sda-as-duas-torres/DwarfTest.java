import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest
{
    @After
    public void tearDown() {
        System.gc();
    }
    
    @Test
    public void dwarfSofre3Flechadas() {
        Dwarf mauricio = new Dwarf("MauMau");
        Elfo legolas = new Elfo("Legolas");

        legolas.atirarFlecha(mauricio);
        legolas.atirarFlecha(mauricio);
        legolas.atirarFlecha(mauricio);

        assertEquals(80, mauricio.getVida(), 0.1);

    }

    @Test
    public void dwarfNasceComVida() {
        Dwarf aline = new Dwarf("Alice");
        assertEquals(Status.VIVO, aline.getStatus());
    }

    @Test
    public void dwarfSemVidaEstaMorto() {
        Dwarf filipe = new Dwarf("Fileps");
        Elfo legolas = new Elfo("Legolas");
        legolas.atirarFlecha(filipe);
        for (int i =0; i < 13; i++) {
            filipe.sofreDano();
        }
        assertEquals(Status.MORTO, filipe.getStatus());   
    }

    @Test
    public void dwarfMortoNaoLevaDano() {
        Dwarf lindice = new Dwarf("Landiceys");
        for (int i =0; i < 13; i++) {
            lindice.sofreDano();
        }
        assertEquals(0, lindice.getVida(), 0.1);
    }

}