import java.util.*;

public class ExercitoDeElfos {  

    private ArrayList<Elfo> exercitoDeElfos = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>(); 
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
            Arrays.asList(
                ElfoVerde.class,
                ElfoNoturno.class
            )
        );

    public void alistarElfo(Elfo elfo) {
        if (estaApto(elfo)) {
            exercitoDeElfos.add(elfo);
            ArrayList<Elfo> elfosPorStatus = porStatus.get(elfo.getStatus());
            this.inicializarListaDeStatus(elfo.getStatus());
            this.porStatus.get(elfo.getStatus()).add(elfo);
        }
    }

    public ArrayList<Elfo> buscarPorStatus(Status status) {
        this.atualizaListaPorStatus();
        return this.porStatus.get(status);
    }
    
    private void inicializarListaDeStatus(Status status) {
        if (this.porStatus.get(status) == null) {
            ArrayList<Elfo> elfosPorStatus = new ArrayList<>();
            porStatus.put(status, elfosPorStatus);
        }
    }

    private void atualizaListaPorStatus() {        
        ArrayList<Elfo> listaDeMortos = new ArrayList<>();
        for (Elfo elfo : this.porStatus.get(Status.VIVO)) {
            if (elfo.getStatus() != Status.VIVO) {
                listaDeMortos.add(elfo);
            }
        }
        
        this.porStatus.get(Status.VIVO).removeAll(listaDeMortos);
        this.inicializarListaDeStatus(Status.MORTO);
        this.porStatus.get(Status.MORTO).addAll(listaDeMortos);
    }

    private boolean estaApto(Elfo elfo) {
        return elfo.getStatus() == Status.VIVO && TIPOS_PERMITIDOS.contains(elfo.getClass());
    }

    public ArrayList<Elfo> getListaExercito() {
        return this.exercitoDeElfos;
    }    
}