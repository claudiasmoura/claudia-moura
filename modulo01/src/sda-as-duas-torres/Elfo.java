public class Elfo extends Personagem 
{
    protected int experiencia;
    protected int experienciaPorFlechada;

    private static int elfosInstanciados = 0;

    public Elfo(String nome){
        super(nome, 100.0, Status.VIVO);
        super.inventario = inventario;
        this.experienciaPorFlechada = 1;
        this.ganharArcoEFlecha();
        elfosInstanciados++;
    }

    public int getExperiencia() {
        return this.experiencia;
    }

    public void atirarFlecha(Dwarf dwarf) {
        if (getQuantidadeFlechas() > 0) {
            this.inventario.buscar("Flecha").setQuantidade(getQuantidadeFlechas() - 1);
            this.experiencia += experienciaPorFlechada;
            this.sofreDano();
            dwarf.sofreDano();
        }
    }

    public int getQuantidadeFlechas() {
        return this.inventario.buscar("Flecha").getQuantidade();
    }

    private void ganharArcoEFlecha() {
        super.ganharItem(new Item("Arco", 1));
        super.ganharItem(new Item("Flecha", 7));
    }

    public static int getElfosInstanciados() {
        return Elfo.elfosInstanciados;
    }

    protected void finalize() throws Throwable {
        Elfo.elfosInstanciados--;
    }
}