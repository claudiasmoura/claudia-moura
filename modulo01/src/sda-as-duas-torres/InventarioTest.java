import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class InventarioTest
{
    @Test
    public void adicionarCanecaIncluiCanecaNoArray() {
        Item caneca = new Item("Caneca bonita", 1);
        Inventario inventario = new Inventario ();
        inventario.adicionar(caneca);
        assertEquals("Caneca bonita", inventario.obter(0).getDescricao());        
    }

    @Test
    public void obterRetornaObjetoNaPosicaoIndicada() {
        Item candelabro = new Item("Lumière", 1);
        Item relogio = new Item("Cogsworth", 1);
        Item xicara = new Item("Chip", 1);
        Item bule = new Item("Sra Potts", 1);
        Item espanador = new Item("Le Plumme", 1);

        Inventario inventario = new Inventario ();

        inventario.adicionar(candelabro);
        inventario.adicionar(relogio);
        inventario.adicionar(xicara);
        inventario.adicionar(bule);
        inventario.adicionar(espanador);

        assertEquals(xicara, inventario.obter(2));        
    }

    @Test
    public void removerTiraDoArrayObjetoNaPosicaoIndicada() {
        Inventario inventario = new Inventario ();
        inventario.adicionar(new Item("Lumière", 1));
        inventario.adicionar(new Item("Cogsworth", 1));
        inventario.adicionar(new Item("Chip", 1));
        inventario.adicionar(new Item("Sra Potts", 1));
        inventario.adicionar(new Item("Le Plumme", 1));

        inventario.remover(2);
        inventario.remover(0);

        assertEquals(3, inventario.inventario.size());
    }

    @Test
    public void removerComItemPorParametroExcluiItemDoInventario() {
        Item candelabro = new Item("Lumière", 1);
        Item relogio = new Item("Cogsworth", 1);
        Item xicara = new Item("Chip", 1);

        Inventario inventario = new Inventario ();

        inventario.adicionar(candelabro);
        inventario.adicionar(relogio);
        inventario.adicionar(xicara);

        inventario.remover(xicara);
        inventario.remover(candelabro);

        assertEquals(1, inventario.inventario.size());
    }

    @Test
    public void getDescricoesItensRetornaDescricoesEmUmaString() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Lumière", 1));
        inventario.adicionar(new Item("Cogsworth", 1));
        inventario.adicionar(new Item("Chip", 1));
        inventario.adicionar(new Item("Sra Potts", 1));
        inventario.adicionar(new Item("Le Plumme", 1));

        inventario.remover(1);

        assertEquals("Lumière,Chip,Sra Potts,Le Plumme", inventario.getDescricoesItens());
    }

    @Test
    public void getDescricoesItensRemovendoItens() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 1);
        Item escudo = new Item("Escudo", 2);
        Item flechas = new Item("Flechas", 3);
        Item xicara = new Item("Chip", 1);

        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(flechas);
        inventario.adicionar(xicara);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(flechas);
        inventario.adicionar(xicara);
        inventario.remover(1);
        inventario.remover(2);
        inventario.remover(3);
        inventario.remover(2);

        String resultado = inventario.getDescricoesItens();
        assertEquals("Espada,Flechas,Flechas,Chip", resultado);
    }

    @Test
    public void retornaItemComMaiorQuantidade() {
        Inventario inventario = new Inventario();

        Item candelabro = new Item("Lumière", 1);
        Item relogio = new Item("Cogsworth", 7);
        Item xicara = new Item("Chip", 2);
        Item bule = new Item("Sra Potts", 4);;

        inventario.adicionar(candelabro);
        inventario.adicionar(relogio);
        inventario.adicionar(xicara);
        inventario.adicionar(bule);

        assertEquals(relogio, inventario.maiorQuantidade());
    }

    @Test
    public void itensComQuantidadesIguaisRetornaOPrimeiroItem() {
        Item candelabro = new Item("Lumière", 1);
        Item espanador = new Item("Le Plumme", 1);
        Item relogio = new Item("Cogsworth", 1);

        Inventario inventario = new Inventario();
        inventario.adicionar(candelabro);
        inventario.adicionar(espanador);
        inventario.adicionar(relogio);        

        assertEquals(candelabro, inventario.maiorQuantidade());
    }

    @Test
    public void maiorQuantidadeComArrayVazioRetornaNull() {
        Inventario inventario = new Inventario();    
        assertEquals(null, inventario.maiorQuantidade());
    }

    @Test
    public void maiorQuantidadeComUmItemRetornaItem() {
        Inventario inventario = new Inventario();

        Item espanador = new Item("Le Plumme", 3);
        inventario.adicionar(espanador);

        assertEquals(espanador, inventario.maiorQuantidade());
    }

    @Test 
    public void buscarChipRetornaItemComDescricaoChip() {
        Item xicara = new Item("Chip", 1);
        Item bule = new Item("Sra Potts", 1);
        Item espanador = new Item("Le Plumme", 1);

        Inventario inventario = new Inventario();

        inventario.adicionar(xicara);
        inventario.adicionar(bule);
        inventario.adicionar(espanador);

        String descricao = "Chip";

        assertEquals(xicara, inventario.buscar(descricao));        
    }

    @Test 
    public void buscarChipRetornaPrimeiroItemComDescricaoChip() {

        Item xicara = new Item("Chip", 1);
        Item xicara2 = new Item("Chip", 5);
        Item bule = new Item("Sra Potts", 1);
        Item espanador = new Item("Le Plumme", 1);

        Inventario inventario = new Inventario();

        inventario.adicionar(xicara);
        inventario.adicionar(xicara2);
        inventario.adicionar(bule);
        inventario.adicionar(espanador);

        String descricao = "Chip";

        assertEquals(xicara, inventario.buscar(descricao));        
    }

    @Test 
    public void buscarLumiereEmListaSemLumiereRetornaNull() {
        Item xicara = new Item("Chip", 1);
        Item bule = new Item("Sra Potts", 1);
        Item espanador = new Item("Le Plumme", 1);

        Inventario inventario = new Inventario();

        inventario.adicionar(xicara);
        inventario.adicionar(bule);
        inventario.adicionar(espanador);
        String descricao = "Lumiere";

        assertEquals(null, inventario.buscar(descricao));        
    }

    @Test 
    public void buscarChipEmListaVaziaRetornaNull() {
        Inventario inventario = new Inventario();
        assertEquals(null, inventario.buscar("Chip"));        
    }

    @Test
    public void inverterRetornaInventarioInvertido() {
        Item xicara = new Item("Chip", 1);
        Item bule = new Item("Sra Potts", 1);
        Item espanador = new Item("Le Plumme", 1);

        Inventario inventario = new Inventario();

        inventario.adicionar(xicara);
        inventario.adicionar(bule);
        inventario.adicionar(espanador);

        assertEquals(espanador, inventario.inverter().get(0));
        assertEquals(bule, inventario.inverter().get(1));
        assertEquals(xicara, inventario.inverter().get(2));
    }

    @Test
    public void inverterNaoAlteraArrayOriginal() {

        Item xicara = new Item("Chip", 1);
        Item bule = new Item("Sra Potts", 1);
        Item espanador = new Item("Le Plumme", 1);

        Inventario inventario = new Inventario();

        inventario.adicionar(xicara);
        inventario.adicionar(bule);
        inventario.adicionar(espanador);

        assertEquals(xicara, inventario.inventario.get(0));
        assertEquals(bule, inventario.inventario.get(1));
        assertEquals(espanador, inventario.inventario.get(2));        
    }

    @Test
    public void inverterComUmItemRetornaUnicoItem() {
        Item xicara = new Item("Chip", 1);
        Inventario inventario = new Inventario();

        inventario.adicionar(xicara);

        assertEquals(xicara, inventario.inventario.get(0));
    }

    @Test
    public void inverterComInventarioVazioRetornaNull() {
        Inventario inventario = new Inventario();

        assertEquals(null, inventario.inverter());
    }

    @Test
    public void ordenaListaComListaTotalmenteDesordenadaRetornaListaOrdenada() {
        Item candelabro = new Item("Lumière", 4);
        Item relogio = new Item("Cogsworth", 6);
        Item xicara = new Item("Chip", 7);
        Item bule = new Item("Sra Potts", 1);
        Item espanador = new Item("Le Plumme", 2);

        Inventario inventario = new Inventario ();

        inventario.adicionar(candelabro);
        inventario.adicionar(relogio);
        inventario.adicionar(xicara);
        inventario.adicionar(bule);
        inventario.adicionar(espanador);

        inventario.ordenarItem();

        assertEquals(bule, inventario.obter(0));
        assertEquals(espanador, inventario.obter(1));
        assertEquals(candelabro, inventario.obter(2));
        assertEquals(relogio, inventario.obter(3));
        assertEquals(xicara, inventario.obter(4));
    }

    @Test
    public void ordenarComListaTotalmenteOrdenadaNaoAlteraInventario() {
        Item bule = new Item("Sra Potts", 1);
        Item espanador = new Item("Le Plumme", 2);
        Item relogio = new Item("Cogsworth", 3);
        Item xicara = new Item("Chip", 5);
        Item candelabro = new Item("Lumière", 5);

        Inventario inventario = new Inventario ();        
        inventario.adicionar(bule);
        inventario.adicionar(espanador);
        inventario.adicionar(relogio);
        inventario.adicionar(xicara);
        inventario.adicionar(candelabro);

        inventario.ordenarItem();

        assertEquals(bule, inventario.obter(0));
        assertEquals(espanador, inventario.obter(1));
        assertEquals(relogio, inventario.obter(2));
        assertEquals(xicara, inventario.obter(3));
        assertEquals(candelabro, inventario.obter(4));
    }

    @Test
    public void ordenaListaComApenasUmItemNaoAlteraLista() {
        Item bule = new Item("Sra Potts", 15);

        Inventario inventario = new Inventario ();        
        inventario.adicionar(bule);        
        inventario.ordenarItem();

        assertEquals(bule, inventario.obter(0));
    }

    @Test
    public void ordenarComParametroAscRetornaListaOrdenadaAscendente() {
        Item candelabro = new Item("Lumière", 4);
        Item relogio = new Item("Cogsworth", 6);
        Item xicara = new Item("Chip", 3);
        Item bule = new Item("Sra Potts", 1);
        Item espanador = new Item("Le Plumme", 2);

        Inventario inventario = new Inventario ();
        inventario.adicionar(candelabro);
        inventario.adicionar(relogio);
        inventario.adicionar(xicara);
        inventario.adicionar(bule);
        inventario.adicionar(espanador);

        inventario.ordenarItem(TipoOrdenacao.ASC);

        assertEquals(bule, inventario.obter(0));
        assertEquals(espanador, inventario.obter(1));
        assertEquals(xicara, inventario.obter(2));
        assertEquals(candelabro, inventario.obter(3));
        assertEquals(relogio, inventario.obter(4));
    }

    @Test
    public void ordenarListaTotalmenteOrdenadaAscendenteComParametroASCNaoAlteraInventario() {
        Item bule = new Item("Sra Potts", 1);
        Item espanador = new Item("Le Plumme", 2);
        Item relogio = new Item("Cogsworth", 3);
        Item xicara = new Item("Chip", 5);
        Item candelabro = new Item("Lumière", 5);

        Inventario inventario = new Inventario ();        
        inventario.adicionar(bule);
        inventario.adicionar(espanador);
        inventario.adicionar(relogio);
        inventario.adicionar(xicara);
        inventario.adicionar(candelabro);

        inventario.ordenarItem(TipoOrdenacao.ASC);

        assertEquals(bule, inventario.obter(0));
        assertEquals(espanador, inventario.obter(1));
        assertEquals(relogio, inventario.obter(2));
        assertEquals(xicara, inventario.obter(3));
        assertEquals(candelabro, inventario.obter(4));
    }

    @Test
    public void ordenaListaComApenasUmItemComParametroASCNaoAlteraLista() {
        Item bule = new Item("Sra Potts", 15);

        Inventario inventario = new Inventario ();        
        inventario.adicionar(bule);        
        inventario.ordenarItem(TipoOrdenacao.ASC);

        assertEquals(bule, inventario.obter(0));
    }

    @Test
    public void ordenarComParametroDescRetornaListaOrdenadaDescendente() {
        Item candelabro = new Item("Lumière", 4);
        Item relogio = new Item("Cogsworth", 6);
        Item xicara = new Item("Chip", 3);
        Item bule = new Item("Sra Potts", 1);
        Item espanador = new Item("Le Plumme", 2);

        Inventario inventario = new Inventario ();

        inventario.adicionar(candelabro);
        inventario.adicionar(relogio);
        inventario.adicionar(xicara);
        inventario.adicionar(bule);
        inventario.adicionar(espanador);

        inventario.ordenarItem(TipoOrdenacao.DESC);

        assertEquals(relogio, inventario.obter(0));
        assertEquals(candelabro, inventario.obter(1));
        assertEquals(xicara, inventario.obter(2));
        assertEquals(espanador, inventario.obter(3));
        assertEquals(bule, inventario.obter(4));        
    }

    public void ordenarListaTotalmenteOrdenadaDescendenteComParametroDESCNaoAlteraInventario() {
        Item candelabro = new Item("Lumière", 5);
        Item xicara = new Item("Chip", 5);
        Item relogio = new Item("Cogsworth", 3);
        Item espanador = new Item("Le Plumme", 2);
        Item bule = new Item("Sra Potts", 1);

        Inventario inventario = new Inventario ();
        inventario.adicionar(candelabro);
        inventario.adicionar(xicara);
        inventario.adicionar(relogio);
        inventario.adicionar(espanador);
        inventario.adicionar(bule);

        inventario.ordenarItem(TipoOrdenacao.DESC);

        assertEquals(candelabro, inventario.obter(0));
        assertEquals(xicara, inventario.obter(1));
        assertEquals(relogio, inventario.obter(2));
        assertEquals(espanador, inventario.obter(3));
        assertEquals(bule, inventario.obter(4));
    }

    @Test
    public void ordenaListaComApenasUmItemComParametroDESCNaoAlteraLista() {
        Item bule = new Item("Sra Potts", 15);

        Inventario inventario = new Inventario ();        
        inventario.adicionar(bule);        
        inventario.ordenarItem(TipoOrdenacao.DESC);

        assertEquals(bule, inventario.obter(0));
    }

    @Test
    public void unirRetornaDoisInventariosEmUm() {
        Item candelabro = new Item("Lumière", 5);
        Item xicara = new Item("Chip", 5);
        Item relogio = new Item("Cogsworth", 3);        

        Inventario inventario1 = new Inventario();
        inventario1.adicionar(candelabro);
        inventario1.adicionar(xicara);
        inventario1.adicionar(relogio);

        Inventario inventario2 = new Inventario();
        Item espanador = new Item("Le Plumme", 2);
        Item bule = new Item("Sra Potts", 1);

        inventario2.adicionar(espanador);
        inventario2.adicionar(bule);

        Inventario inventarioUnido = new Inventario();

        inventarioUnido =inventario1.unir(inventario2);

        assertEquals(5, inventarioUnido.getItens().size());
        assertTrue(inventarioUnido.getItens().contains(espanador));
        assertTrue(inventarioUnido.getItens().contains(xicara));

    }

    @Test
    public void unirNaoModificaInventarioQueChamaOMetodo() {
        Item candelabro = new Item("Lumière", 5);
        Item xicara = new Item("Chip", 5);
        Item relogio = new Item("Cogsworth", 3);        

        Inventario inventario1 = new Inventario();
        inventario1.adicionar(candelabro);
        inventario1.adicionar(xicara);
        inventario1.adicionar(relogio);

        Inventario inventario2 = new Inventario();
        Item espanador = new Item("Le Plumme", 2);
        Item bule = new Item("Sra Potts", 1);

        inventario2.adicionar(espanador);
        inventario2.adicionar(bule);

        inventario1.unir(inventario2);

        assertEquals(3, inventario1.getItens().size());
        assertTrue(inventario1.getItens().contains(candelabro));
        assertFalse(inventario1.getItens().contains(bule));        
    }

    @Test
    public void unirNaoModificaInventarioPassadoPorParametro() {
        Item candelabro = new Item("Lumière", 5);
        Item xicara = new Item("Chip", 5);
        Item relogio = new Item("Cogsworth", 3);        

        Inventario inventario1 = new Inventario();
        inventario1.adicionar(candelabro);
        inventario1.adicionar(xicara);
        inventario1.adicionar(relogio);

        Inventario inventario2 = new Inventario();
        Item espanador = new Item("Le Plumme", 2);
        Item bule = new Item("Sra Potts", 1);

        inventario2.adicionar(espanador);
        inventario2.adicionar(bule);

        inventario1.unir(inventario2);

        assertEquals(2, inventario2.getItens().size());
        assertTrue(inventario2.getItens().contains(espanador));
        assertFalse(inventario2.getItens().contains(relogio));        
    }

    @Test
    public void diferenciarRetornaInventarioComItensDiferentes() {
        Item candelabro = new Item("Lumière", 5);
        Item xicara = new Item("Chip", 5);
        Item relogio = new Item("Cogsworth", 3);        
        Item espanador = new Item("Le Plumme", 2);
        Item bule = new Item("Sra Potts", 1);

        Inventario inventario1 = new Inventario();
        inventario1.adicionar(candelabro);
        inventario1.adicionar(espanador);
        inventario1.adicionar(xicara);
        inventario1.adicionar(relogio);

        Inventario inventario2 = new Inventario();
        inventario2.adicionar(relogio);
        inventario2.adicionar(espanador);
        inventario2.adicionar(bule);

        Inventario inventarioDiferente = new Inventario();

        inventarioDiferente = inventario1.diferenciar(inventario2);

        assertTrue(inventarioDiferente.getItens().contains(candelabro));
        assertTrue(inventarioDiferente.getItens().contains(xicara));
        assertFalse(inventarioDiferente.getItens().contains(relogio));
        assertFalse(inventarioDiferente.getItens().contains(espanador));

    }

    @Test
    public void diferenciarNaoModificaInventarioQueChamaOMetodo() {
        Item candelabro = new Item("Lumière", 5);
        Item xicara = new Item("Chip", 5);
        Item relogio = new Item("Cogsworth", 3);        

        Inventario inventario1 = new Inventario();
        inventario1.adicionar(candelabro);
        inventario1.adicionar(xicara);
        inventario1.adicionar(relogio);

        Inventario inventario2 = new Inventario();
        Item espanador = new Item("Le Plumme", 2);
        Item bule = new Item("Sra Potts", 1);

        inventario2.adicionar(espanador);
        inventario2.adicionar(bule);
        inventario2.adicionar(xicara);
        inventario2.adicionar(relogio);

        inventario1.diferenciar(inventario2);

        assertEquals(3, inventario1.getItens().size());
        assertTrue(inventario1.getItens().contains(candelabro));
        assertFalse(inventario1.getItens().contains(bule));        
    }

    @Test
    public void diferenciarNaoModificaInventarioPassadoPorParametro() {
        Item candelabro = new Item("Lumière", 5);
        Item xicara = new Item("Chip", 5);
        Item relogio = new Item("Cogsworth", 3);        

        Inventario inventario1 = new Inventario();
        inventario1.adicionar(candelabro);
        inventario1.adicionar(xicara);
        inventario1.adicionar(relogio);

        Inventario inventario2 = new Inventario();
        Item espanador = new Item("Le Plumme", 2);
        Item bule = new Item("Sra Potts", 1);

        inventario2.adicionar(espanador);
        inventario2.adicionar(xicara);
        inventario2.adicionar(bule);

        inventario1.diferenciar(inventario2);

        assertEquals(3, inventario2.getItens().size());
        assertTrue(inventario2.getItens().contains(espanador));
        assertFalse(inventario2.getItens().contains(relogio));        
    }

    @Test
    public void cruzarRetornaInventarioComItensRepetidos() {
        Item candelabro = new Item("Lumière", 5);
        Item xicara = new Item("Chip", 5);
        Item relogio = new Item("Cogsworth", 3);        
        Item espanador = new Item("Le Plumme", 2);
        Item bule = new Item("Sra Potts", 1);

        Inventario inventario1 = new Inventario();
        inventario1.adicionar(candelabro);
        inventario1.adicionar(espanador);
        inventario1.adicionar(xicara);
        inventario1.adicionar(relogio);

        Inventario inventario2 = new Inventario();
        inventario2.adicionar(relogio);
        inventario2.adicionar(espanador);
        inventario2.adicionar(bule);
        inventario2.adicionar(candelabro);

        Inventario inventarioCruzado = new Inventario();

        inventarioCruzado = inventario1.cruzar(inventario2);        
        
        assertEquals(3, inventarioCruzado.getItens().size());
        assertTrue(inventarioCruzado.getItens().contains(candelabro));
        assertTrue(inventarioCruzado.getItens().contains(relogio));
        assertTrue(inventarioCruzado.getItens().contains(espanador));
        assertFalse(inventarioCruzado.getItens().contains(xicara));
        assertFalse(inventarioCruzado.getItens().contains(bule));
    }

    @Test
    public void cruzarNaoModificaInventarioQueChamaOMetodo() {
        Item candelabro = new Item("Lumière", 5);
        Item xicara = new Item("Chip", 5);
        Item relogio = new Item("Cogsworth", 3);        

        Inventario inventario1 = new Inventario();
        inventario1.adicionar(candelabro);
        inventario1.adicionar(xicara);

        Inventario inventario2 = new Inventario();
        Item espanador = new Item("Le Plumme", 2);
        Item bule = new Item("Sra Potts", 1);

        inventario2.adicionar(espanador);
        inventario2.adicionar(bule);

        inventario1.cruzar(inventario2);

        assertEquals(2, inventario1.getItens().size());
        assertTrue(inventario1.getItens().contains(candelabro));
        assertTrue(inventario1.getItens().contains(xicara));
        assertFalse(inventario1.getItens().contains(bule)); 
        assertFalse(inventario1.getItens().contains(espanador));
    }

}
