public class DwarfBarbaLonga extends Dwarf {
    private String nome;
    private Dado dado;

    public DwarfBarbaLonga(String nome, Dado dado) {
        super(nome);
        this.dado = dado;
    }

    @Override
    public void sofreDano() {
        if (this.dado.sortear() <= 4) {
            super.sofreDano();
        }
    }
}