public class ElfoDaLuz extends Elfo
{
    private int qtdeGolpes = 0;
    private static final double qtdeVidaGanha = 10.;

    public ElfoDaLuz(String nome) {
        super(nome);
        this.ganharEspadaDeGalvorn();
        this.qtdeDano = 21;
    }

    public void atacarComEspada(Dwarf dwarf) {
        if (this.status == Status.MORTO) return;
        dwarf.sofreDano();
        this.qtdeGolpes++;
        
        if (devePerderVida()) {
            this.sofreDano();            
            return;
        }
        this.ganhaVida(qtdeVidaGanha);        
    }

    private boolean devePerderVida() {
        return this.qtdeGolpes % 2 == 1;
    }

    private void ganhaVida(double qtdeVida) {
        this.vida += qtdeVida;
    }

    private void ganharEspadaDeGalvorn() {
        this.inventario.adicionar(new Item("Espada de Galvorn", 1));        
    }
    
    @Override
    public void perderItem(Item item) {
        if (possoPerderItem(item)) {
           super.perderItem(item); 
        }
    }
    
    private boolean possoPerderItem(Item item) {
        return !item.getDescricao().equals("Espada de Galvorn");
    }
}