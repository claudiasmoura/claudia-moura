import java.util.*;

public class AtaqueIntercalado implements Estrategia {
    private HashMap<Class, ArrayList<Elfo>> porClasse = new HashMap<>();
    private ArrayList<Class> classes = new ArrayList<>(Arrays.asList(ElfoVerde.class, ElfoNoturno.class));
    private Dado dado;
    private int valor; 

    public AtaqueIntercalado(Dado dado) {
        this.valor = dado.sortear();
    }

    public List<Elfo> getOrdemDeAtaque(List<Elfo> atacantes) {
        this.adicionarElfosAHashMap(atacantes);
        ArrayList<Elfo> ordenados = new ArrayList<>();

        for (int i = 0; i < this.tamanhoDaLista(); i++) {
            ordenados.add(porClasse.get(classes.get(valor % 2)).get(i));
            ordenados.add(porClasse.get(classes.get((valor + 1) % 2)).get(i));
        }

        return ordenados;
    }

    private void adicionarElfosAHashMap(List<Elfo> atacantes) {
        for (Elfo elfo : atacantes) {
            if (elfo.getStatus() == Status.VIVO) {
                this.inicializarListaDeClasse(elfo.getClass());
                porClasse.get(elfo.getClass()).add(elfo);
            }
        }
    }

    private void inicializarListaDeClasse(Class classe) {
        if (listaDeClasseVazia(classe)) {
            ArrayList<Elfo> elfosPorClasse = new ArrayList<>();
            porClasse.put(classe, elfosPorClasse);
        }
    }

    private boolean listaDeClasseVazia(Class classe) {
        return porClasse.get(classe) == null;
    }

    private int tamanhoDaLista() {
        //ta confuso mas funciona ¯\_(ツ)_/¯
        int tamanhoListaElfoVerde = this.porClasse.get(ElfoVerde.class).size();
        int tamanhoListaElfoNoturno = this.porClasse.get(ElfoNoturno.class).size();
        int tamanho = tamanhoListaElfoVerde < tamanhoListaElfoNoturno ? tamanhoListaElfoVerde :tamanhoListaElfoNoturno;
        return tamanho;
    }
}
