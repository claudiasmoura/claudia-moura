public class ElfoVerde extends Elfo
{
    String nome; 

    public ElfoVerde(String nome) {
        super(nome);
        this.experienciaPorFlechada = 2;
    }

    public void ganharItem(Item item) {
        if (this.itemPermitido(item)) {
            this.inventario.adicionar(item);
        }
    }

    private boolean itemPermitido(Item item) {
        if (item.descricao.equals("Espada de aço valiriano") ||
        item.descricao.equals("Arco de Vidro") ||
        item.descricao.equals("Flecha de Vidro")) 
            return true;

        return false;
    }
}