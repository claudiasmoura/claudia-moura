import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class AtaqueIntercaladoTest
{   
    @Test
    public void dadoParComecaAtaquePorVerdes() {
        Dado dado = new DadoFake(2);
        
        Elfo yasser = new ElfoNoturno("Mad Yasser");
        Elfo rachid = new ElfoNoturno("Yace");
        Elfo ghilherme = new ElfoVerde("GG");        
        Elfo henrique = new ElfoVerde("GhiGhi");

        ArrayList<Elfo> elfos = new ArrayList(
                Arrays.asList(yasser, rachid, ghilherme, henrique)
            );
            
        Estrategia estrategia = new AtaqueIntercalado(dado);
        ArrayList<Elfo> esperado = new ArrayList(
                Arrays.asList(ghilherme, yasser, henrique, rachid)
            );
        assertEquals(esperado, estrategia.getOrdemDeAtaque(elfos));
    }
    
    @Test
    public void dadoImarComecaAtaquePorNoturnos() {
        Dado dado = new DadoFake(3);
        
        Elfo yasser = new ElfoNoturno("Mad Yasser");
        Elfo rachid = new ElfoNoturno("Yace");
        Elfo ghilherme = new ElfoVerde("GG");        
        Elfo henrique = new ElfoVerde("GhiGhi");

        ArrayList<Elfo> elfos = new ArrayList(
                Arrays.asList(yasser, rachid, ghilherme, henrique)
            );
            
        Estrategia estrategia = new AtaqueIntercalado(dado);
        ArrayList<Elfo> esperado = new ArrayList(
                Arrays.asList(yasser, ghilherme, rachid, henrique)
            );
        assertEquals(esperado, estrategia.getOrdemDeAtaque(elfos));
    }
    
    @Test
    public void intercalaComMaisElfosNoturnos() {
        Dado dado = new DadoFake(5);
        
        Elfo yasser = new ElfoNoturno("Mad Yasser");
        Elfo rachid = new ElfoNoturno("Yace");
        Elfo mauricio = new ElfoNoturno("Mau");
        Elfo amanda = new ElfoNoturno("Amandita");        
        Elfo ghilherme = new ElfoVerde("GG");        
        Elfo henrique = new ElfoVerde("GhiGhi");

        ArrayList<Elfo> elfos = new ArrayList(
                Arrays.asList(yasser, rachid, mauricio, amanda, ghilherme, henrique)
            );
            
        Estrategia estrategia = new AtaqueIntercalado(dado);
        ArrayList<Elfo> esperado = new ArrayList(
                Arrays.asList(yasser, ghilherme, rachid, henrique)
            );
        assertEquals(esperado, estrategia.getOrdemDeAtaque(elfos));
    }
    
    @Test
    public void intercalaComMaisElfosVerdes() {
        Dado dado = new DadoFake(1);
        
        Elfo yasser = new ElfoNoturno("Mad Yasser");
        Elfo rachid = new ElfoNoturno("Yace");
        Elfo mauricio = new ElfoVerde("Mau");
        Elfo amanda = new ElfoVerde("Amandita");        
        Elfo ghilherme = new ElfoVerde("GG");        
        Elfo henrique = new ElfoVerde("GhiGhi");

        ArrayList<Elfo> elfos = new ArrayList(
                Arrays.asList(yasser, rachid, mauricio, amanda, ghilherme, henrique)
            );
            
        Estrategia estrategia = new AtaqueIntercalado(dado);
        ArrayList<Elfo> esperado = new ArrayList(
                Arrays.asList(yasser, mauricio, rachid, amanda)
            );
        assertEquals(esperado, estrategia.getOrdemDeAtaque(elfos));
    }    
}