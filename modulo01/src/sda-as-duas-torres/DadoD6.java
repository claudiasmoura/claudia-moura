import java.util.Random;

public class DadoD6 implements Dado
{
    private int qtdFaces = 6;

    public int sortear() {
        return new Random().nextInt(qtdFaces) + 1;
    }
}
