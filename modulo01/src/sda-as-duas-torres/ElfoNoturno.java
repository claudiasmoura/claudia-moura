public class ElfoNoturno extends Elfo
{
    String nome; 
    public ElfoNoturno(String nome) {
        super(nome);
        this.experienciaPorFlechada *= 3;
        this.qtdeDano = 15;
    }
}