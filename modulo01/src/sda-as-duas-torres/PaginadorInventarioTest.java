import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class PaginadorInventarioTest
{
    @Test
    public void paginadorRetornaItensNoIntervaloIndicado() {

        Item espada = new Item("Espada", 1);
        Item escudo = new Item("Escudo de metal", 2);
        Item pocao = new Item("Poção de HP", 3);
        Item bracelete = new Item("Bracelete", 1);

        Inventario inventario = new Inventario();
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(pocao);
        inventario.adicionar(bracelete);

        PaginadorInventario paginador = new PaginadorInventario(inventario);

        paginador.pular(0);
        assertEquals(2, paginador.limitar(2).size());
        assertEquals(espada, paginador.limitar(2).get(0));        
        assertEquals(escudo, paginador.limitar(2).get(1));  

        paginador.pular(1);
        assertEquals(3, paginador.limitar(3).size());
        assertEquals(escudo, paginador.limitar(3).get(0));  
        assertEquals(pocao, paginador.limitar(3).get(1));        
        assertEquals(bracelete, paginador.limitar(3).get(2));
    }
    
    @Test 
    public void utilizarLimitarSemDefinirPularRetornaNull() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Espada", 1));

        PaginadorInventario paginador = new PaginadorInventario(inventario);

        assertEquals(null, paginador.limitar(2));
    }
    
    @Test 
    public void pularComValorMaiorQueTamanhoDoInventarioRetornaNull() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Espada", 1));

        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(3);
        
        assertEquals(null, paginador.limitar(2));
    }
    
    @Test 
    public void limitarComValorMaiorQueTamanhoDoInventarioRetornaNull() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Espada", 1));

        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        
        assertEquals(null, paginador.limitar(3));
    }
    
    @Test 
    public void paginarComInventarioVazioRetornaNull() {
        Inventario inventario = new Inventario();
        
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        
        assertEquals(null, paginador.limitar(0));
    }
    
    @Test
    public void limitarMenorQuePularRetornaNull() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Espada", 1));
        inventario.adicionar(new Item("Escudo de metal", 2));
        inventario.adicionar(new Item("Poção de HP", 3));

        PaginadorInventario paginador = new PaginadorInventario(inventario);

        paginador.pular(2);
        assertEquals(null, paginador.limitar(1));
    }
    
}
