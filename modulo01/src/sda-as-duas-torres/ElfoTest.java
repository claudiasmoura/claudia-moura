import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest
{
        @After
    public void tearDown() {
        System.gc();
    }
    
    @Test
    public void criarElfoComNome() {
        Elfo legolas = new Elfo("Legolas");
        assertEquals("Legolas", legolas.getNome());
    }

    @Test    
    public void atirar3FlechasDiminui3Flechas() {
        Dwarf marcosPaulo = new Dwarf("Marquinhos");
        Elfo legolas = new Elfo("Legolas");
        legolas.atirarFlecha(marcosPaulo);
        legolas.atirarFlecha(marcosPaulo);
        legolas.atirarFlecha(marcosPaulo);

        assertEquals(4, legolas.getQuantidadeFlechas());
    }

    @Test
    public void atirar3FlechasAumentaExperienciaEm3(){
        Dwarf marcosPaulo = new Dwarf("Marquinhos");
        Elfo legolas = new Elfo("Legolas");
        legolas.atirarFlecha(marcosPaulo);
        legolas.atirarFlecha(marcosPaulo);
        legolas.atirarFlecha(marcosPaulo);

        assertEquals(3, legolas.getExperiencia());
    }

    @Test
    public void atirarFlechasCom0FlechasNaoProvocaAlteracoesEmExperiencia(){
        Dwarf marcosPaulo = new Dwarf("Marquinhos");
        Elfo legolas = new Elfo("Legolas");

        for (int i = 0; i <=7; i++) {
            legolas.atirarFlecha(marcosPaulo);
        }

        legolas.atirarFlecha(marcosPaulo);
        legolas.atirarFlecha(marcosPaulo);
        legolas.atirarFlecha(marcosPaulo);

        assertEquals(7, legolas.getExperiencia());
    }
    
    @Test
    public void ganharFlechasAumentaQuantidadeDasJaExistentes(){
        Elfo legolas = new Elfo("Legolas");        
        legolas.ganharItem(new Item("Flecha", 10));

        assertEquals(17, legolas.getQuantidadeFlechas());
    }
    
    @Test
    public void itensRepetidosAcrescentamNaQuantidade() {
        Elfo legolas = new Elfo("Legolas");        
        legolas.ganharItem(new Item("Espada", 1));
        legolas.ganharItem(new Item("Arco", 2));
        legolas.ganharItem(new Item("Espada", 3));
        legolas.ganharItem(new Item("Flecha", 5));
        legolas.ganharItem(new Item("Espada", 6));

        assertEquals(3, legolas.getInventario().getItens().size());
    }
        
    @Test
    public void atirarFlechasCom0FlechasNaoProvocaAlteracoesEmQtdFlechas(){
        Dwarf marcosPaulo = new Dwarf("Marquinhos");
        Elfo legolas = new Elfo("Legolas");

        for (int i = 0; i <=7; i++) {
            legolas.atirarFlecha(marcosPaulo);
        }

        legolas.atirarFlecha(marcosPaulo);
        legolas.atirarFlecha(marcosPaulo);
        legolas.atirarFlecha(marcosPaulo);        

        assertEquals(0, legolas.getQuantidadeFlechas());
    }

    //@Test
    public void elfosInstanciadosAcrescentamAoContador() {
        assertEquals(0, Elfo.getElfosInstanciados());
        Elfo legolas = new Elfo("Legolas");
        Elfo legolas2 = new ElfoVerde("Legolas");
        Elfo legolas3 = new ElfoNoturno("Legolas");
        Elfo legolas4 = new ElfoDaLuz("Legolas");

        assertEquals(4, Elfo.getElfosInstanciados());

        Elfo legolas5 = new Elfo("Legolas");
        Elfo legolas6 = new ElfoVerde("Legolas");
        Elfo legolas7 = new ElfoNoturno("Legolas");
        Elfo legolas8 = new ElfoDaLuz("Legolas");

        assertEquals(8, Elfo.getElfosInstanciados());
    }
    
    //@Test
    public void personagemsNaoElfosNaoAcrescentamAoContador() {
        Personagem dwarf = new DwarfBarbaLonga("Dwarf", new DadoD6());
        Personagem dwarf2 = new Dwarf("Dwarf");
        Personagem dwarf3 = new DwarfBarbaLonga("Dwarf", new DadoD6());
        Personagem dwarf4 = new Dwarf("Dwarf");

        assertEquals(0, Elfo.getElfosInstanciados());
    }
}
