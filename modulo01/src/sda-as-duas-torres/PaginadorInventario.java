import java.util.ArrayList;

public class PaginadorInventario 
{
    private Inventario inventario;
    private int inicio = -1;
    
    public PaginadorInventario(Inventario inventario) {
        this.inventario = inventario;
        this.inicio = inicio;
    }
    
    public void pular(int primeiroItem) {
        if (primeiroItem > this.inventario.getItens().size()) {
            return;
        }
        this.inicio = primeiroItem;
    }
    
    public ArrayList<Item> limitar(int qtdeItens) {        
        if (inicio < 0 || qtdeItens < 0 || qtdeItens < inicio || inventario.inventario.isEmpty()) return null;
        
        ArrayList<Item> inventarioLimitado = new ArrayList<>();
        int ultimoItem = this.inicio + qtdeItens;
        
        if (ultimoItem > inventario.getItens().size()) return null;
        
        for (int i = this.inicio; i < ultimoItem; i++) {
            inventarioLimitado.add(inventario.getItens().get(i));
        }
        
        return inventarioLimitado;
    }
}