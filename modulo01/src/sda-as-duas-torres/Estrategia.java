import java.util.List;

public interface Estrategia {

    public List<Elfo> getOrdemDeAtaque(List<Elfo> atacantes);

}
