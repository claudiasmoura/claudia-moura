public class ItemImperdivel extends Item {
    public ItemImperdivel (String descricao, int quantidade) {        
        super(descricao, quantidade);    
    }
    
    @Override
    public void setQuantidade(int novaQuantidade) {
        boolean podeAlterar = novaQuantidade > 0;
        this.quantidade = podeAlterar ? novaQuantidade : 1;
    }
}