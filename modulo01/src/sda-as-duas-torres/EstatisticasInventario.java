import java.util.ArrayList;

public class EstatisticasInventario
{
    Inventario inventario;

    public EstatisticasInventario(Inventario inventario) {
        this.inventario = inventario;
    }

    public double calcularMedia() {
        if (this.inventario.getItens().isEmpty()) return Double.NaN;

        double soma = 0.;         
        for (Item item: this.inventario.inventario) {
            soma += item.getQuantidade();  
        }
        return soma / this.inventario.getItens().size();
    }

    public double calcularMediana() {
        if (this.inventario.getItens().isEmpty()) return Double.NaN;
        this.inventario.ordenarItem();
        
        double mediana;
        int tamanhoInventario = this.inventario.getItens().size();

        if (tamanhoInventario == 1) return this.inventario.obter(0).getQuantidade();

        ArrayList<Item> inventarioOrdenado = new ArrayList<>(this.inventario.inventario);
        if (tamanhoInventario % 2 == 0) {
            int qtdeItem1 = inventarioOrdenado.get((tamanhoInventario / 2)).getQuantidade();
            int qtdeItem2 = inventarioOrdenado.get((tamanhoInventario / 2 - 1)).getQuantidade();

            return mediana = (qtdeItem1 + qtdeItem2) / 2.;
        }

        return this.inventario.obter((tamanhoInventario - 1) / 2).getQuantidade();        
    }

    public int qtdItensAcimaDaMedia() {
        if (this.inventario.getItens().isEmpty()) return 0;

        int qtdeItens = 0; 
        double media = calcularMedia();
        for (Item item: this.inventario.getItens()) {
            if (item.getQuantidade() > media) qtdeItens++;
        }
        return qtdeItens;
    }
}