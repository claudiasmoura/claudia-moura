import static org.junit.Assert.*;
import java.util.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoDeElfosTest
{
    @After
    public void tearDown() {
        System.gc();
    }
    
    @Test
    public void elfoVerdeEstaAptoASeAlistar() {
        ElfoVerde ghilherme = new ElfoVerde("GG");
        ExercitoDeElfos exercito = new ExercitoDeElfos();

        exercito.alistarElfo(ghilherme);

        assertTrue(exercito.getListaExercito().contains(ghilherme));
    }

    @Test
    public void elfoNoturnoEstaAptoASeAlistar() {
        ElfoNoturno yasser = new ElfoNoturno("Mad Yasser");
        ExercitoDeElfos exercito = new ExercitoDeElfos();

        exercito.alistarElfo(yasser);

        assertTrue(exercito.getListaExercito().contains(yasser));
    }

    @Test
    public void elfosNaoAptosNaoPodemSeAlistar() {
        Elfo amanda = new ElfoDaLuz("Amandita");
        ExercitoDeElfos exercito = new ExercitoDeElfos();

        exercito.alistarElfo(amanda);

        assertFalse(exercito.getListaExercito().contains(amanda));
    }

    @Test
    public void buscaPorElfosVivosRetornaApenasElfosVivos() {
        Elfo yasser = new ElfoNoturno("Mad Yasser");
        Elfo ghilherme = new ElfoVerde("GG");
        Elfo rachid = new ElfoNoturno("Yace");
        Elfo henrique = new ElfoVerde("GhiGhi");

        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistarElfo(yasser);
        exercito.alistarElfo(ghilherme);
        exercito.alistarElfo(rachid);
        exercito.alistarElfo(henrique);

        for (int i = 0; i < 9; i++) {
            yasser.atirarFlecha(new Dwarf("Dudu"));
            rachid.atirarFlecha(new Dwarf("Dudu"));
        }
        
        assertEquals(2, exercito.buscarPorStatus(Status.VIVO).size());
        assertFalse(exercito.buscarPorStatus(Status.VIVO).contains(yasser));       
        assertFalse(exercito.buscarPorStatus(Status.VIVO).contains(rachid));
    }

    @Test
    public void buscaPorElfosMortosRetornaApenasElfosMortos() {
        Elfo yasser = new ElfoNoturno("Mad Yasser");
        Elfo ghilherme = new ElfoNoturno("GG");
        Elfo rachid = new ElfoNoturno("Yace");
        Elfo henrique = new ElfoVerde("GhiGhi");

        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistarElfo(yasser);
        exercito.alistarElfo(ghilherme);
        exercito.alistarElfo(rachid);
        exercito.alistarElfo(henrique);

        for (int i = 0; i < 9; i++) {
            yasser.atirarFlecha(new Dwarf("Dudu"));
            rachid.atirarFlecha(new Dwarf("Dudu"));
            ghilherme.atirarFlecha(new Dwarf("Dudu"));
        }

        assertEquals(3, exercito.buscarPorStatus(Status.MORTO).size());
        assertTrue(exercito.buscarPorStatus(Status.MORTO).contains(yasser));
        assertTrue(exercito.buscarPorStatus(Status.MORTO).contains(rachid));
        assertTrue(exercito.buscarPorStatus(Status.MORTO).contains(ghilherme));
    }
}