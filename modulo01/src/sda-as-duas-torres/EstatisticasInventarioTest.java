import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest
{
    @Test
    public void retornaMediaDeInventario() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("sapatenis de vinil", 5));
        inventario.adicionar(new Item("bolsinha baguete", 3));
        inventario.adicionar(new Item("luvinha de pelica", 8));

        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(5.3, estatisticas.calcularMedia(), 0.1);
    }

    @Test
    public void mediaDeInventarioVazioRetornaNaN() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);

        assertEquals(Double.NaN, estatisticas.calcularMedia(), 0);
    }

    @Test
    public void retornaMedianaDeItensDeInventarioComQuantidadePar() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("vinil", 10));
        inventario.adicionar(new Item("baguete", 6));
        inventario.adicionar(new Item("pelica", 3));
        inventario.adicionar(new Item("chick", 2));

        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);

        assertEquals(4.5, estatisticas.calcularMediana(), 0.1);
    }

    @Test
    public void retornaMedianaDeItensDeInventarioComQuantidadeImpar() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("vinil", 1));
        inventario.adicionar(new Item("baguete", 6));
        inventario.adicionar(new Item("pelica", 3));
        inventario.adicionar(new Item("chick", 10));
        inventario.adicionar(new Item("agreste", 2));

        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);

        assertEquals(3, estatisticas.calcularMediana(), 0.1);
    }

    @Test
    public void retornaMedianaDeItensDeInventarioComUmItem() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("sapatenis de vinil", 10));

        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);

        assertEquals(10, estatisticas.calcularMediana(), 0.1);
    }

    @Test
    public void medianaDeInventarioVazioRetornaNaN() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);

        assertEquals(Double.NaN, estatisticas.calcularMediana(), 0);
    }

    @Test
    public void retornaQuantidadeDeItensAcimaDaMedia() {
        Inventario inventario = new Inventario();

        inventario.adicionar(new Item("sapatenis de vinil", 1));
        inventario.adicionar(new Item("bolsinha baguete", 2));
        inventario.adicionar(new Item("luvinha de pelica", 8));

        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);

        assertEquals(1, estatisticas.qtdItensAcimaDaMedia());
    }
    
    @Test
    public void itensNaMediaNaoEstaoAcimaDaMedia() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("vinil", 1));
        inventario.adicionar(new Item("baguete", 3));
        inventario.adicionar(new Item("pelica", 6));
        inventario.adicionar(new Item("chick", 10));
        inventario.adicionar(new Item("agreste", 10));

        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);

        assertEquals(2, estatisticas.qtdItensAcimaDaMedia());
    }
    
    @Test
    public void qtdItensAcimaDaMediaComInventarioVazioRetornaZero() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);

        assertEquals(0, estatisticas.qtdItensAcimaDaMedia());
    }
}
