public class Dwarf extends Personagem
{
    private String nome;
    private double vida;
    private Status status;

    public Dwarf(String nome) {
        super(nome, 110.0, Status.VIVO);
        this.qtdeDano = 10;
    }
}