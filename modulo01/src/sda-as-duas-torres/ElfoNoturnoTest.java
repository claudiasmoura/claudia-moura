import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest
{
    @After
    public void tearDown() {
        System.gc();
    }
    
    @Test
    public void elfoNoturnoAtirarFlechasGanha9DeExperiencia() {
        ElfoNoturno yasser = new ElfoNoturno("Yasser");
        Dwarf fernanda = new Dwarf("Fe");

        yasser.atirarFlecha(fernanda);
        yasser.atirarFlecha(fernanda);
        yasser.atirarFlecha(fernanda);

        assertEquals(9, yasser.getExperiencia(), 0.1);
    }

    @Test
    public void elfoNoturnoAtira3FlechasPerde45DeVida() {
        ElfoNoturno yasser = new ElfoNoturno("Yasser");
        Dwarf fernanda = new Dwarf("Fe");

        yasser.atirarFlecha(fernanda);
        yasser.atirarFlecha(fernanda);
        yasser.atirarFlecha(fernanda);

        assertEquals(55, yasser.getVida(), 0.1);
    }

    @Test
    public void elfoNoturnoMorreSeChegarAZeroDeVida() {
        ElfoNoturno yasser = new ElfoNoturno("Yasser");
        Dwarf desiree = new Dwarf("Desi");

        for (int i = 0; i < 7; i++) {
            yasser.atirarFlecha(desiree);
        }

        assertEquals(Status.MORTO, yasser.getStatus());
    }

    @Test
    public void elfoNoturnoMortoNaoAtiraFlecha() {
        ElfoNoturno yasser = new ElfoNoturno("Yasser");
        Dwarf luiz = new Dwarf("Luis");
        
        for (int i = 0; i < 8; i++) {
            yasser.atirarFlecha(luiz);
            double x = yasser.getExperiencia();
        }
        assertEquals(21, yasser.getExperiencia());
    }
}
