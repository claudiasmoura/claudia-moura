public class DadoFake implements Dado {
    private int valor; 
    
    public DadoFake(int numero) {
        this.valor = numero;
    }

    public int sortear() {
        return this.valor;
    }
}
