public abstract class Personagem {
    protected String nome;
    protected double vida;
    protected double qtdeDano;
    protected Status status;
    protected Inventario inventario;
    public boolean mudouDeStatus;

    public Personagem(String nome, double vida, Status status){
        this.nome = nome;
        this.vida = vida;
        this.status = Status.VIVO;
        this.inventario = new Inventario();
        this.qtdeDano = 0;
    }

    public String getNome() {
        return this.nome;
    }

    public double getVida() {
        return this.vida;
    }

    public Status getStatus() {
        return this.status;
    }

    public Inventario getInventario(){
        return this.inventario;
    }

    public void ganharItem(Item item) {
        Item itemNoInventario = this.inventario.buscar(item.getDescricao());
        if (itemNoInventario != null) {
            itemNoInventario.setQuantidade(item.quantidade + itemNoInventario.getQuantidade());
            return;
        } 
        this.inventario.adicionar(item);
    }

    public void perderItem(Item item) {
        inventario.remover(item);
    }

    public void sofreDano() {
        this.vida -= this.qtdeDano;
        if (this.vida <= 0) {            
            morre();
        }        
    } 

    private void morre() {
        this.status = status.MORTO;
        this.vida = 0;
    }    
    
}